%% template.sty
%% Repository: <https://gitlab.com/nithiya/books>
%% Copyright 2019-2021 N. M. Streethran (nmstreethran at gmail dot com)
%
% This work is licensed under the terms of the MIT License.
% <https://opensource.org/licenses/MIT>
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% avoid overfulls in right margin
\emergencystretch3em

% date formatting
\usepackage[british]{datetime2}

% new font size bigger than /Huge!
\usepackage{fix-cm}
\makeatletter
\newcommand\HUGE{\@setfontsize\Huge{50}{30}}
\makeatother

% main font
\usepackage{ebgaramond}

% custom colours
\usepackage[x11names]{xcolor}

% drop caps
\usepackage{lettrine}
\setlength{\DefaultFindent}{3pt}
\setlength{\DefaultNindent}{0pt}
\setcounter{DefaultLines}{3}
\renewcommand{\LettrineTextFont}{\scshape\lowercase}

% verses / quotes
\usepackage{verse}
% modify quote environment
\usepackage{etoolbox}
\AtBeginEnvironment{quote}{\itshape\Large}
\AtBeginEnvironment{verse}{\itshape\Large}

% modify ToC style
\usepackage[titles]{tocloft}
\cftpagenumbersoff{part}
\renewcommand\cftchappagefont{\mdseries}
% disable dots
\renewcommand{\cftdotsep}{\cftnodots}
% modify part and chapter entries
\makeatletter
\patchcmd{\l@part}{#1}
  {\mdseries\scshape\LARGE\lowercase{\hfil\hspace{\@pnumwidth}#1\hfil}}{}{}
\patchcmd{\l@chapter}{#1}{\mdseries\scshape\lowercase{#1}}{}{}
\makeatother

% section fonts
\usepackage{titlesec}
\titleformat{\section}{\LARGE\scshape}{}{0em}{\lowercase}
\titleformat{\subsection}[runin]{\Large\scshape}{}{0em}{\lowercase}
\titleformat{\subsubsection}[runin]{\scshape}{\subsubsection}{0em}{\lowercase}
\titleformat{\paragraph}[runin]{\itshape}{\paragraph}{0em}{}

% modify paragraph skip length
\setlength{\parskip}{\baselineskip}

% multiple columns
\usepackage{multicol}

% modify lists
\usepackage{enumitem}

% metadata
\title{\textbf\booktitle}
\author{\authorname}
\date{}
\def\bookinfo{Title: \booktitle \\
  Author: \authorname \\
  \ifdefined\illustrator
  Illustrator: \illustrator \\
  \fi
  Release Date: \releasedate \\
  eBook number: \ebooknumber \\
  \ifdefined\postingdate
  Posting Date: \postingdate \\
  \fi
  \ifdefined\lastupdated
  Last Updated: \lastupdated \\
  \fi
  Producer(s): \producers \\
  Compilation Date: \today
}

% reset footnote numbering
\usepackage{chngcntr}
\counterwithout{footnote}{chapter}

%% graphics
\usepackage{graphicx}
\graphicspath{{../../images/}}
\usepackage{caption}

% hyperlinks and PDF metadata
\usepackage[hidelinks,unicode]{hyperref}
\hypersetup{
  pdfauthor={\authorname},
  pdftitle={\booktitle},
  pdfkeywords={\keywords},
  pdfsubject={Project Gutenberg (www.gutenberg.org) eBook No. \ebooknumber.}
}
% don't use monospace font for URLs
\urlstyle{same}

% margins
\usepackage[a4paper]{geometry}
\geometry{lmargin=3cm,rmargin=1.5cm,tmargin=2.5cm,bmargin=2.5cm}

% header and footer
\usepackage{fancyhdr}
\pagestyle{fancy}
% footer for first page of chapter
\fancypagestyle{plain}{
  % sets both header and footer to nothing
  \fancyhf{}
  % remove horizontal line from header
  \renewcommand{\headrulewidth}{0pt}
  % page number
  \fancyfoot[C]{\large\thepage}
}
% header and footer for all other pages
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyhead[LE,RO]{\large\thepage}
\fancyhead[CE]{\large\scshape\MakeLowercase{\booktitle}}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
% disable ToC chaptermark uppercase
\patchcmd{\tableofcontents}{\MakeUppercase\contentsname}{\contentsname}{}{}
\patchcmd{\tableofcontents}{\MakeUppercase\contentsname}{\contentsname}{}{}
\fancyhead[CO]{\MakeLowercase{\large\scshape\leftmark}}

%% front matter
% title page, ToC and information
\newcommand{\thefrontmatter}{
  \frontmatter

  \begin{titlepage}
    % horizontal centering
    \centering
    \thetitlepage
  \end{titlepage}

  \Large

  % vertical centering
  \hspace{0pt} % ^
  \vfill % ^
  \noindent The Project Gutenberg eBook of \booktitle, by \authorname

  \noindent
  This eBook is for the use of anyone anywhere in the United States and
  most other parts of the world at no cost and with almost no
  restrictions whatsoever. You may copy it, give it away or re-use it
  under the terms of the Project Gutenberg License included with this
  eBook or online at \href{https://www.gutenberg.org}{www.gutenberg.org}. If
  you are not located in the United States, you'll have to check the laws of
  the country where you are located before using this eBook.

  \noindent\bookinfo
  % vertical centering
  \vfill % ^
  \hspace{0pt} % ^
  \cleardoublepage

  \ifdefined\preface
  \preface
  \fi

  \tableofcontents
  \cleardoublepage

  \ifdefined\introduction
  \introduction
  \fi

  \mainmatter
}

%% back matter
% Project Gutenberg license
\newcommand{\fulllicense}{
  \cleardoublepage
  \chapter{Project Gutenberg License}

  Creating the works from print editions not protected by U.S.
  copyright law means that no one owns a United States copyright in
  these works, so the Foundation (and you!) can copy and distribute it
  in the United States without permission and without paying copyright
  royalties. Special rules, set forth in the General Terms of Use part
  of this license, apply to copying and distributing Project
  Gutenberg-tm electronic works to protect the
  \MakeLowercase{\textsc{PROJECT GUTENBERG}}-tm
  concept and trademark. Project Gutenberg is a registered trademark,
  and may not be used if you charge for the eBooks, unless you receive
  specific permission. If you do not charge anything for copies of this
  eBook, complying with the rules is very easy. You may use this eBook
  for nearly any purpose such as creation of derivative works, reports,
  performances and research. They may be modified and printed and given
  away--you may do practically \MakeLowercase{\textsc{ANYTHING}} in the
  United States with
  eBooks not protected by U.S. copyright law. Redistribution is subject
  to the trademark license, especially commercial redistribution.

  \noindent\MakeLowercase{\textsc{START: FULL LICENSE}}

  \noindent\MakeLowercase{\textsc{THE FULL PROJECT GUTENBERG LICENSE. PLEASE
  READ THIS BEFORE YOU DISTRIBUTE OR USE THIS WORK.}}

  To protect the Project Gutenberg-tm mission of promoting the free
  distribution of electronic works, by using or distributing this work
  (or any other work associated in any way with the phrase ``Project
  Gutenberg''), you agree to comply with all the terms of the Full
  Project Gutenberg-tm License available with this file or online at
  \href{https://www.gutenberg.org/policy/permission.html}
  {www.gutenberg.org/policy/permission}.

  \section*{Section 1. General Terms of Use and Redistributing Project
  Gutenberg-tm electronic works}

  \subsection*{1.A.}
  By reading or using any part of this Project Gutenberg-tm electronic
  work, you indicate that you have read, understand, agree to and
  accept all the terms of this license and intellectual property
  (trademark/copyright) agreement. If you do not agree to abide by all
  the terms of this agreement, you must cease using and return or
  destroy all copies of Project Gutenberg-tm electronic works in your
  possession. If you paid a fee for obtaining a copy of or access to a
  Project Gutenberg-tm electronic work and you do not agree to be bound
  by the terms of this agreement, you may obtain a refund from the
  person or entity to whom you paid the fee as set forth in paragraph
  \MakeLowercase{\textsc{1.E.8}}.

  \subsection*{1.B.}
  ``Project Gutenberg'' is a registered trademark. It may only be used
  on or associated in any way with an electronic work by people who
  agree to be bound by the terms of this agreement. There are a few
  things that you can do with most Project Gutenberg-tm electronic
  works even without complying with the full terms of this agreement.
  See paragraph \MakeLowercase{\textsc{1.C}} below. There are a lot of
  things you can do with
  Project Gutenberg-tm electronic works if you follow the terms of this
  agreement and help preserve free future access to Project
  Gutenberg-tm electronic works. See paragraph
  \MakeLowercase{\textsc{1.E}} below.

  \subsection*{1.C.}
  The Project Gutenberg Literary Archive Foundation (``the Foundation''
  or PGLAF), owns a compilation copyright in the collection of Project
  Gutenberg-tm electronic works. Nearly all the individual works in the
  collection are in the public domain in the United States. If an
  individual work is unprotected by copyright law in the United States
  and you are located in the United States, we do not claim a right to
  prevent you from copying, distributing, performing, displaying or
  creating derivative works based on the work as long as all references
  to Project Gutenberg are removed. Of course, we hope that you will
  support the Project Gutenberg-tm mission of promoting free access to
  electronic works by freely sharing Project Gutenberg-tm works in
  compliance with the terms of this agreement for keeping the Project
  Gutenberg-tm name associated with the work. You can easily comply
  with the terms of this agreement by keeping this work in the same
  format with its attached full Project Gutenberg-tm License when you
  share it without charge with others.

  \subsection*{1.D.}
  The copyright laws of the place where you are located also govern
  what you can do with this work. Copyright laws in most countries are
  in a constant state of change. If you are outside the United States,
  check the laws of your country in addition to the terms of this
  agreement before downloading, copying, displaying, performing,
  distributing or creating derivative works based on this work or any
  other Project Gutenberg-tm work. The Foundation makes no
  representations concerning the copyright status of any work in any
  country outside the United States.

  \subsection*{1.E.}
  Unless you have removed all references to Project Gutenberg:

  \subsubsection*{1.E.1.}
  The following sentence, with active links to, or other immediate
  access to, the full Project Gutenberg-tm License must appear
  prominently whenever any copy of a Project Gutenberg-tm work (any
  work on which the phrase ``Project Gutenberg'' appears, or with which
  the phrase ``Project Gutenberg'' is associated) is accessed,
  displayed, performed, viewed, copied or distributed:

  \begin{quote}
    This eBook is for the use of anyone anywhere in the United States
    and most other parts of the world at no cost and with almost no
    restrictions whatsoever. You may copy it, give it away or re-use
    it under the terms of the Project Gutenberg License included with
    this eBook or online at www.gutenberg.org. If you are not located
    in the United States, you'll have to check the laws of the
    country where you are located before using this ebook.
  \end{quote}

  \subsubsection*{1.E.2.}
  If an individual Project Gutenberg-tm electronic work is derived from
  texts not protected by U.S. copyright law (does not contain a notice
  indicating that it is posted with permission of the copyright 
  holder), the work can be copied and distributed to anyone in the
  United States without paying any fees or charges. If you are
  redistributing or providing access to a work with the phrase
  ``Project Gutenberg'' associated with or appearing on the work, you
  must comply either with the requirements of paragraphs 
  \MakeLowercase{\textsc{1.E.1}} through \MakeLowercase{\textsc{1.E.7}}
  or obtain permission for the use of the work and the Project
  Gutenberg-tm trademark as set forth in paragraphs
  \MakeLowercase{\textsc{1.E.8}} or \MakeLowercase{\textsc{1.E.9}}.

  \subsubsection*{1.E.3.}
  If an individual Project Gutenberg-tm electronic work is posted with
  the permission of the copyright holder, your use and distribution
  must comply with both paragraphs \MakeLowercase{\textsc{1.E.1}} through
  \MakeLowercase{\textsc{1.E.7}} and any
  additional terms imposed by the copyright holder. Additional terms
  will be linked to the Project Gutenberg-tm License for all works
  posted with the permission of the copyright holder found at the
  beginning of this work.

  \subsubsection*{1.E.4.}
  Do not unlink or detach or remove the full Project Gutenberg-tm
  License terms from this work, or any files containing a part of this
  work or any other work associated with Project Gutenberg-tm.

  \subsubsection*{1.E.5.}
  Do not copy, display, perform, distribute or redistribute this
  electronic work, or any part of this electronic work, without
  prominently displaying the sentence set forth in paragraph
  \MakeLowercase{\textsc{1.E.1}} with
  active links or immediate access to the full terms of the Project
  Gutenberg-tm License.

  \subsubsection*{1.E.6.}
  You may convert to and distribute this work in any binary,
  compressed, marked up, nonproprietary or proprietary form, including
  any word processing or hypertext form. However, if you provide access
  to or distribute copies of a Project Gutenberg-tm work in a format
  other than ``Plain Vanilla ASCII'' or other format used in the
  official version posted on the official Project Gutenberg-tm web site
  (\href{https://www.gutenberg.org}{www.gutenberg.org}), you must, at no
  additional cost, fee or expense to the user, provide a copy, a means of
  exporting a copy, or a means of obtaining a copy upon request, of the work
  in its original ``Plain Vanilla ASCII'' or other form. Any alternate format
  must include the full Project Gutenberg-tm License as specified in
  paragraph \MakeLowercase{\textsc{1.E.1}}.

  \subsubsection*{1.E.7.}
  Do not charge a fee for access to, viewing, displaying, performing,
  copying or distributing any Project Gutenberg-tm works unless you
  comply with paragraph \MakeLowercase{\textsc{1.E.8}} or
  \MakeLowercase{\textsc{1.E.9}}.

  \subsubsection*{1.E.8.}
  You may charge a reasonable fee for copies of or providing access to
  or distributing Project Gutenberg-tm electronic works provided that

  \begin{itemize}
    \item You pay a royalty fee of 20\,\% of the gross profits you
    derive from the use of Project Gutenberg-tm works calculated
    using the method you already use to calculate your applicable
    taxes. The fee is owed to the owner of the Project Gutenberg-tm
    trademark, but he has agreed to donate royalties under this
    paragraph to the Project tenberg Literary Archive Foundation.
    Royalty payments must be paid within 60 days following each date
    on which you prepare (or are legally required to prepare) your
    periodic tax returns. Royalty payments should be clearly marked
    as such and sent to the Project Gutenberg Literary Archive
    Foundation at the address specified in Section 4,
    ``Information about donations to the Project Gutenberg Literary
    Archive Foundation.''
    \item You provide a full refund of any money paid by a user who
    notifies you in writing (or by e-mail) within 30 days of receipt
    that s/he does not agree to the terms of the full Project
    Gutenberg-tm License. You must require such a user to return or
    destroy all copies of the works possessed in a physical medium
    and discontinue all use of and all access to other copies of
    Project Gutenberg-tm works.
    \item You provide, in accordance with paragraph
    \MakeLowercase{\textsc{1.F.3}}, a full
    refund of any money paid for a work or a replacement copy, if a
    defect in the electronic work is discovered and reported to you
    within 90 days of receipt of the work.
    \item You comply with all other terms of this agreement for free
    distribution of Project Gutenberg-tm works.
  \end{itemize}

  \subsubsection*{1.E.9.}
  If you wish to charge a fee or distribute a Project Gutenberg-tm
  electronic work or group of works on different terms than are set
  forth in this agreement, you must obtain permission in writing from
  both the Project Gutenberg Literary Archive Foundation and The
  Project Gutenberg Trademark LLC, the owner of the Project
  Gutenberg-tm trademark. Contact the Foundation as set forth in
  Section 3 below.

  \subsection*{1.F.}

  \subsubsection*{1.F.1.}
  Project Gutenberg volunteers and employees expend considerable effort
  to identify, do copyright research on, transcribe and proofread works
  not protected by U.S. copyright law in creating the Project
  Gutenberg-tm collection. Despite these efforts, Project Gutenberg-tm
  electronic works, and the medium on which they may be stored, may
  contain ``Defects,'' such as, but not limited to, incomplete,
  inaccurate or corrupt data, transcription errors, a copyright or
  other intellectual property infringement, a defective or damaged disk
  or other medium, a computer virus, or computer codes that damage or
  cannot be read by your equipment.

  \subsubsection*{1.F.2.}
  \MakeLowercase{\textsc{LIMITED WARRANTY, DISCLAIMER OF DAMAGES}}
  - Except for the ``Right of
  Replacement or Refund'' described in paragraph
  \MakeLowercase{\textsc{1.F.3}}, the Project
  Gutenberg Literary Archive Foundation, the owner of the Project
  Gutenberg-tm trademark, and any other party distributing a Project
  Gutenberg-tm electronic work under this agreement, disclaim all
  liability to you for damages, costs and expenses, including legal
  fees. \MakeLowercase{\textsc{YOU AGREE THAT YOU HAVE NO REMEDIES FOR
  NEGLIGENCE, STRICT LIABILITY, BREACH OF WARRANTY OR BREACH OF CONTRACT
  EXCEPT THOSE PROVIDED IN PARAGRAPH 1.F.3. YOU AGREE THAT THE FOUNDATION,
  THE TRADEMARK OWNER, AND ANY DISTRIBUTOR UNDER THIS AGREEMENT WILL NOT BE
  LIABLE TO YOU FOR ACTUAL, DIRECT, INDIRECT, CONSEQUENTIAL, PUNITIVE
  OR INCIDENTAL DAMAGES EVEN IF YOU GIVE NOTICE OF THE POSSIBILITY OF
  SUCH DAMAGE.}}

  \subsubsection*{1.F.3.}
  \MakeLowercase{\textsc{LIMITED RIGHT OF REPLACEMENT OR REFUND}} - If you
  discover a defect in
  this electronic work within 90 days of receiving it, you can receive
  a refund of the money (if any) you paid for it by sending a written
  explanation to the person you received the work from. If you received
  the work on a physical medium, you must return the medium with your
  written explanation. The person or entity that provided you with the
  defective work may elect to provide a replacement copy in lieu of a
  refund. If you received the work electronically, the person or entity
  providing it to you may choose to give you a second opportunity to
  receive the work electronically in lieu of a refund. If the second
  copy is also defective, you may demand a refund in writing without
  further opportunities to fix the problem.

  \subsubsection*{1.F.4.}
  Except for the limited right of replacement or refund set forth in
  paragraph \MakeLowercase{\textsc{1.F.3}}, this work is provided to you
  \MakeLowercase{\textsc{`AS-IS', WITH NO OTHER
  WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
  TO WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PURPOSE.}}

  \subsubsection*{1.F.5.}
  Some states do not allow disclaimers of certain implied warranties or
  the exclusion or limitation of certain types of damages. If any
  disclaimer or limitation set forth in this agreement violates the law
  of the state applicable to this agreement, the agreement shall be
  interpreted to make the maximum disclaimer or limitation permitted by
  the applicable state law. The invalidity or unenforceability of any
  provision of this agreement shall not void the remaining provisions.

  \subsubsection*{1.F.6.}
  \MakeLowercase{\textsc{INDEMNITY}}
  - You agree to indemnify and hold the Foundation, the
  trademark owner, any agent or employee of the Foundation, anyone
  providing copies of Project Gutenberg-tm electronic works in
  accordance with this agreement, and any volunteers associated with
  the production, promotion and distribution of Project Gutenberg-tm
  electronic works, harmless from all liability, costs and expenses,
  including legal fees, that arise directly or indirectly from any of
  the following which you do or cause to occur: (a) distribution of
  this or any Project Gutenberg-tm work, (b) alteration, modification,
  or additions or deletions to any Project Gutenberg-tm work, and (c)
  any Defect you cause.

  \section*{Section 2. Information about the Mission of Project Gutenberg-tm}

  Project Gutenberg-tm is synonymous with the free distribution of
  electronic works in formats readable by the widest variety of
  computers including obsolete, old, middle-aged and new computers. It
  exists because of the efforts of hundreds of volunteers and donations
  from people in all walks of life.

  Volunteers and financial support to provide volunteers with the
  assistance they need are critical to reaching Project Gutenberg-tm's
  goals and ensuring that the Project Gutenberg-tm collection will
  remain freely available for generations to come. In 2001, the Project
  Gutenberg Literary Archive Foundation was created to provide a secure
  and permanent future for Project Gutenberg-tm and future generations.
  To learn more about the Project Gutenberg Literary Archive Foundation
  and how your efforts and donations can help, see Sections 3 and 4 and
  the Foundation information page at
  \href{https://www.gutenberg.org}{www.gutenberg.org}.

  \section*{Section 3. Information about the Project Gutenberg Literary
  Archive Foundation}

  The Project Gutenberg Literary Archive Foundation is a non profit
  501(c)(3) educational corporation organized under the laws of the
  state of Mississippi and granted tax exempt status by the Internal
  Revenue Service. The Foundation's EIN or federal tax identification
  number is 64-6221541. Contributions to the Project Gutenberg Literary
  Archive Foundation are tax deductible to the full extent permitted by
  U.S. federal laws and your state's laws.

  The Foundation's principal office is in Fairbanks, Alaska, with the
  mailing address: PO Box 750175, Fairbanks, AK 99775, but its
  volunteers and employees are scattered throughout numerous locations.
  Its business office is located at 809 North 1500 West, Salt Lake
  City, UT 84116, (801) 596-1887. Email contact links and up to date
  contact information can be found at the Foundation's web site and
  official page at
  \href{https://www.gutenberg.org/about/contact_information.html}
  {www.gutenberg.org/about/contact\_information}.

  For additional contact information:

  \begin{quote}
    Dr. Gregory B. Newby \\
    Chief Executive and Director \\
    gbnewby@pglaf.org
  \end{quote}

  \section*{Section 4. Information about Donations to the Project Gutenberg
  Literary Archive Foundation}

  Project Gutenberg-tm depends upon and cannot survive without wide
  spread public support and donations to carry out its mission of
  increasing the number of public domain and licensed works that can be
  freely distributed in machine readable form accessible by the widest
  array of equipment including outdated equipment. Many small donations
  (\$\,1 to \$\,5,000) are particularly important to maintaining tax exempt
  status with the IRS.

  The Foundation is committed to complying with the laws regulating
  charities and charitable donations in all 50 states of the United
  States. Compliance requirements are not uniform and it takes a
  considerable effort, much paperwork and many fees to meet and keep up
  with these requirements. We do not solicit donations in locations
  where we have not received written confirmation of compliance. To
  \MakeLowercase{\textsc{SEND DONATIONS}} or determine the status of
  compliance for any particular state visit
  \href{https://www.gutenberg.org/donate/}{www.gutenberg.org/donate}.

  While we cannot and do not solicit contributions from states where we
  have not met the solicitation requirements, we know of no prohibition
  against accepting unsolicited donations from donors in such states
  who approach us with offers to donate.

  International donations are gratefully accepted, but we cannot make
  any statements concerning tax treatment of donations received from
  outside the United States. U.S. laws alone swamp our small staff.

  Please check the Project Gutenberg Web pages for current donation
  methods and addresses. Donations are accepted in a number of other
  ways including checks, online payments and credit card donations. To
  donate, please visit:
  \href{https://www.gutenberg.org/donate/}{www.gutenberg.org/donate}.

  \section*{Section 5. General Information About Project Gutenberg-tm
  electronic works}

  Professor Michael S. Hart was the originator of the Project
  Gutenberg-tm concept of a library of electronic works that could be
  freely shared with anyone. For forty years, he produced and
  distributed Project Gutenberg-tm eBooks with only a loose network of
  volunteer support.

  Project Gutenberg-tm eBooks are often created from several printed
  editions, all of which are confirmed as not protected by copyright in
  the U.S. unless a copyright notice is included. Thus, we do not
  necessarily keep eBooks in compliance with any particular paper
  edition.

  Most people start at our Web site which has the main PG search
  facility: \href{https://www.gutenberg.org}{www.gutenberg.org}.

  This Web site includes information about Project Gutenberg-tm,
  including how to make donations to the Project Gutenberg Literary
  Archive Foundation, how to help produce our new eBooks, and how to
  subscribe to our email newsletter to hear about new eBooks.
}
