\chapter{Notes}

\section*{Note to Chapter I} \label{noteCI}

Note A.--The Ranger or the Forest, that cuts the foreclaws off our dogs.

A most sensible grievance of those aggrieved times were the Forest Laws.
These oppressive enactments were the produce of the Norman Conquest, for
the Saxon laws of the chase were mild and humane; while those of
William, enthusiastically attached to the exercise and its rights, were
to the last degree tyrannical. The formation of the New Forest, bears
evidence to his passion for hunting, where he reduced many a happy
village to the condition of that one commemorated by my friend, Mr
William Stewart Rose:

\begin{verse}
``Amongst the ruins of the church\\
The midnight raven found a perch,\\
A melancholy place;\\
The ruthless Conqueror cast down,\\
Woe worth the deed, that little town,\\
To lengthen out his chase.''\\!
\end{verse}

The disabling dogs, which might be necessary for keeping flocks and
herds, from running at the deer, was called ``lawing'', and was in
general use. The Charter of the Forest designed to lessen those evils,
declares that inquisition, or view, for lawing dogs, shall be made every
third year, and shall be then done by the view and testimony of lawful
men, not otherwise; and they whose dogs shall be then found unlawed,
shall give three shillings for mercy, and for the future no man's ox
shall be taken for lawing. Such lawing also shall be done by the assize
commonly used, and which is, that three claws shall be cut off without
the ball of the right foot. See on this subject the Historical Essay on
the Magna Charta of King John, (a most beautiful volume), by Richard
Thomson.

\section*{Note to Chapter II} \label{noteCII}

Note B.--Negro Slaves.

The severe accuracy of some critics has objected to the complexion of
the slaves of Brian de Bois-Guilbert, as being totally out of costume
and propriety. I remember the same objection being made to a set of
sable functionaries, whom my friend, Mat Lewis, introduced as the guards
and mischief-doing satellites of the wicked Baron, in his Castle
Spectre. Mat treated the objection with great contempt, and averred in
reply, that he made the slaves black in order to obtain a striking
effect of contrast, and that, could he have derived a similar advantage
from making his heroine blue, blue she should have been.

I do not pretend to plead the immunities of my order so highly as this;
but neither will I allow that the author of a modern antique romance is
obliged to confine himself to the introduction of those manners only
which can be proved to have absolutely existed in the times he is
depicting, so that he restrain himself to such as are plausible and
natural, and contain no obvious anachronism. In this point of view, what
can be more natural, than that the Templars, who, we know, copied
closely the luxuries of the Asiatic warriors with whom they fought,
should use the service of the enslaved Africans, whom the fate of war
transferred to new masters? I am sure, if there are no precise proofs of
their having done so, there is nothing, on the other hand, that can
entitle us positively to conclude that they never did. Besides, there is
an instance in romance.

John of Rampayne, an excellent juggler and minstrel, undertook to effect
the escape of one Audulf de Bracy, by presenting himself in disguise at
the court of the king, where he was confined. For this purpose, ``he
stained his hair and his whole body entirely as black as jet, so that
nothing was white but his teeth,'' and succeeded in imposing himself on
the king, as an Ethiopian minstrel. He effected, by stratagem, the
escape of the prisoner. Negroes, therefore, must have been known in
England in the dark ages.\footnote{Dissertation on Romance and Minstrelsy,
prefixed to
Ritson's Ancient Metrical Romances, p.~clxxxvii.}

\section*{Note to Chapter XVII} \label{noteCXVII}

Note C.--Minstrelsy.

The realm of France, it is well known, was divided betwixt the Norman
and Teutonic race, who spoke the language in which the word Yes is
pronounced as ``oui'', and the inhabitants of the southern regions,
whose speech bearing some affinity to the Italian, pronounced the same
word ``oc''. The poets of the former race were called ``Minstrels'', and
their poems ``Lays'': those of the latter were termed ``Troubadours'',
and their compositions called ``sirventes'', and other names. Richard, a
professed admirer of the joyous science in all its branches, could
imitate either the minstrel or troubadour. It is less likely that he
should have been able to compose or sing an English ballad; yet so much
do we wish to assimilate Him of the Lion Heart to the band of warriors
whom he led, that the anachronism, if there be one may readily be
forgiven.

\section*{Note to Chapter XXI} \label{noteCXXI}

Note D.--Battle of Stamford.

A great topographical blunder occurred here in former editions. The
bloody battle alluded to in the text, fought and won by King Harold,
over his brother the rebellious Tosti, and an auxiliary force of Danes
or Norsemen, was said, in the text, and a corresponding note, to have
taken place at Stamford, in Leicestershire, and upon the river Welland.
This is a mistake, into which the author has been led by trusting to his
memory, and so confounding two places of the same name. The Stamford,
Strangford, or Staneford, at which the battle really was fought, is a
ford upon the river Derwent, at the distance of about seven miles from
York, and situated in that large and opulent county. A long wooden
bridge over the Derwent, the site of which, with one remaining buttress,
is still shown to the curious traveller, was furiously contested. One
Norwegian long defended it by his single arm, and was at length pierced
with a spear thrust through the planks of the bridge from a boat
beneath.

The neighbourhood of Stamford, on the Derwent, contains some memorials
of the battle. Horseshoes, swords, and the heads of halberds, or bills,
are often found there; one place is called the ``Danes' well,'' another
the ``Battle flats.'' From a tradition that the weapon with which the
Norwegian champion was slain, resembled a pear, or, as others say, that
the trough or boat in which the soldier floated under the bridge to
strike the blow, had such a shape, the country people usually begin a
great market, which is held at Stamford, with an entertainment called
the Pear-pie feast, which after all may be a corruption of the Spear-pie
feast. For more particulars, Drake's History of York may be referred to.
The author's mistake was pointed out to him, in the most obliging
manner, by Robert Belt, Esq. of Bossal House. The battle was fought in
1066.

\section*{Note to Chapter XXII} \label{noteCXXII}

Note E.--The range of iron bars above that glowing charcoal.

This horrid species of torture may remind the reader of that to which
the Spaniards subjected Guatimozin, in order to extort a discovery of
his concealed wealth. But, in fact, an instance of similar barbarity is
to be found nearer home, and occurs in the annals of Queen Mary's time,
containing so many other examples of atrocity. Every reader must
recollect, that after the fall of the Catholic Church, and the
Presbyterian Church Government had been established by law, the rank,
and especially the wealth, of the Bishops, Abbots, Priors, and so forth,
were no longer vested in ecclesiastics, but in lay impropriators of the
church revenues, or, as the Scottish lawyers called them, titulars of
the temporalities of the benefice, though having no claim to the
spiritual character of their predecessors in office.

Of these laymen, who were thus invested with ecclesiastical revenues,
some were men of high birth and rank, like the famous Lord James
Stewart, the Prior of St Andrews, who did not fail to keep for their own
use the rents, lands, and revenues of the church. But if, on the other
hand, the titulars were men of inferior importance, who had been
inducted into the office by the interest of some powerful person, it was
generally understood that the new Abbot should grant for his patron's
benefit such leases and conveyances of the church lands and tithes as
might afford their protector the lion's share of the booty. This was the
origin of those who were wittily termed Tulchan\footnote{A ``Tulchan''
is a calf's skin stuffed, and placed
before a cow who has lost its calf, to induce the animal to part with
her milk. The resemblance between such a Tulchan and a Bishop named to
transmit the temporalities of a benefice to some powerful patron, is
easily understood.} Bishops, being a sort of imaginary prelate, whose
image was set up to
enable his patron and principal to plunder the benefice under his name.

There were other cases, however, in which men who had got grants of
these secularised benefices, were desirous of retaining them for their
own use, without having the influence sufficient to establish their
purpose; and these became frequently unable to protect themselves,
however unwilling to submit to the exactions of the feudal tyrant of the
district.

Bannatyne, secretary to John Knox, recounts a singular course of
oppression practised on one of those titulars abbots, by the Earl of
Cassilis in Ayrshire, whose extent of feudal influence was so wide that
he was usually termed the King of Carrick. We give the fact as it occurs
in Bannatyne's Journal, only premising that the Journalist held his
master's opinions, both with respect to the Earl of Cassilis as an
opposer of the king's party, and as being a detester of the practice of
granting church revenues to titulars, instead of their being devoted to
pious uses, such as the support of the clergy, expense of schools, and
the relief of the national poor. He mingles in the narrative, therefore,
a well deserved feeling of execration against the tyrant who employed
the torture, which a tone of ridicule towards the patient, as if, after
all, it had not been ill bestowed on such an equivocal and amphibious
character as a titular abbot. He entitles his narrative,

\noindent\MakeLowercase{\scshape THE EARL OF CASSILIS' TYRANNY AGAINST A
QUICK (i.e.~LIVING) MAN.}

``Master Allan Stewart, friend to Captain James Stewart of Cardonall, by
means of the Queen's corrupted court, obtained the Abbey of Crossraguel.
The said Earl thinking himself greater than any king in those quarters,
determined to have that whole benefice (as he hath divers others) to pay
at his pleasure; and because he could not find sic security as his
insatiable appetite required, this shift was devised. The said Mr Allan
being in company with the Laird of Bargany, (also a Kennedy,) was, by
the Earl and his friends, enticed to leave the safeguard which he had
with the Laird, and come to make good cheer with the said Earl. The
simplicity of the imprudent man was suddenly abused; and so he passed
his time with them certain days, which he did in Maybole with Thomas
Kennedie, uncle to the said Earl; after which the said Mr Allan passed,
with quiet company, to visit the place and bounds of Crossraguel, {[}his
abbacy,{]} of which the said Earl being surely advertised, determined to
put in practice the tyranny which long before he had conceived. And so,
as king of the country, apprehended the said Mr Allan, and carried him
to the house of Denure, where for a season he was honourably treated,
(if a prisoner can think any entertainment pleasing;) but after that
certain days were spent, and that the Earl could not obtain the feus of
Crossraguel according to his own appetite, he determined to prove if a
collation could work that which neither dinner nor supper could do for a
long time. And so the said Mr Allan was carried to a secret chamber:
with him passed the honourable Earl, his worshipful brother, and such as
were appointed to be servants at that banquet. In the chamber there was
a grit iron chimlay, under it a fire; other grit provision was not seen.
The first course was,--`My Lord Abbot,' (said the Earl,) `it will please
you confess here, that with your own consent you remain in my company,
because ye durst not commit yourself to the hands of others.' The Abbot
answered, `Would you, my lord, that I should make a manifest lie for
your pleasure? The truth is, my lord, it is against my will that I am
here; neither yet have I any pleasure in your company.' `But ye shall
remain with me, nevertheless, at this time,' said the Earl. `I am not
able to resist your will and pleasure,' said the Abbot, `in this place.'
`Ye must then obey me,' said the Earl,--and with that were presented
unto him certain letters to subscribe, amongst which there was a five
years' tack, and a nineteen years' tack, and a charter of feu of all the
lands (of Crossraguel), with all the clauses necessary for the Earl to
haste him to hell. For if adultery, sacrilege, oppression, barbarous
cruelty, and theft heaped upon theft, deserve hell, the great King of
Carrick can no more escape hell for ever, than the imprudent Abbot
escaped the fire for a season as follows.

``After that the Earl spied repugnance, and saw that he could not come
to his purpose by fair means, he commanded his cooks to prepare the
banquet: and so first they flayed the sheep, that is, they took off the
Abbot's cloathes even to his skin, and next they bound him to the
chimney--his legs to the one end, and his arms to the other; and so they
began to beet {[}i.e.~feed{]} the fire sometimes to his buttocks,
sometimes to his legs, sometimes to his shoulders and arms; and that the
roast might not burn, but that it might rest in soppe, they spared not
flambing with oil, (basting as a cook bastes roasted meat); Lord, look
thou to sic cruelty! And that the crying of the miserable man should not
be heard, they dosed his mouth that the voice might be stopped. It may
be suspected that some partisan of the King's {[}Darnley's{]} murder was
there. In that torment they held the poor man, till that often he cried
for God's sake to dispatch him; for he had as meikle gold in his awin
purse as would buy powder enough to shorten his pain. The famous King of
Carrick and his cooks perceiving the roast to be aneuch, commanded it to
be tane fra the fire, and the Earl himself began the grace in this
manner:--`Benedicite, Jesus Maria, you are the most obstinate man that
ever I saw; gif I had known that ye had been so stubborn, I would not
for a thousand crowns have handled you so; I never did so to man before
you.' And yet he returned to the same practice within two days, and
ceased not till that he obtained his formost purpose, that is, that he
had got all his pieces subscryvit alsweill as ane half-roasted hand
could do it. The Earl thinking himself sure enough so long as he had the
half-roasted Abbot in his own keeping, and yet being ashamed of his
presence by reason of his former cruelty, left the place of Denure in
the hands of certain of his servants, and the half-roasted Abbot to be
kept there as prisoner. The Laird of Bargany, out of whose company the
said Abbot had been enticed, understanding, (not the extremity,) but the
retaining of the man, sent to the court, and raised letters of
deliverance of the person of the man according to the order, which being
disobeyed, the said Earl for his contempt was denounced rebel, and put
to the horne. But yet hope was there none, neither to the afflicted to
be delivered, neither yet to the purchaser {[}i.e.~procurer{]} of the
letters to obtain any comfort thereby; for in that time God was
despised, and the lawful authority was contemned in Scotland, in hope of
the sudden return and regiment of that cruel murderer of her awin
husband, of whose lords the said Earl was called one; and yet, oftener
than once, he was solemnly sworn to the King and to his Regent.''

The Journalist then recites the complaint of the injured Allan Stewart,
Commendator of Crossraguel, to the Regent and Privy Council, averring
his having been carried, partly by flattery, partly by force, to the
black vault of Denure, a strong fortalice, built on a rock overhanging
the Irish channel, where to execute leases and conveyances of the whole
churches and parsonages belonging to the Abbey of Crossraguel, which he
utterly refused as an unreasonable demand, and the more so that he had
already conveyed them to John Stewart of Cardonah, by whose interest he
had been made Commendator. The complainant proceeds to state, that he
was, after many menaces, stript, bound, and his limbs exposed to fire in
the manner already described, till, compelled by excess of agony, he
subscribed the charter and leases presented to him, of the contents of
which he was totally ignorant. A few days afterwards, being again
required to execute a ratification of these deeds before a notary and
witnesses, and refusing to do so, he was once more subjected to the same
torture, until his agony was so excessive that he exclaimed, ``Fye on
you, why do you not strike your whingers into me, or blow me up with a
barrel of powder, rather than torture me thus unmercifully?'' upon which
the Earl commanded Alexander Richard, one of his attendants, to stop the
patient's mouth with a napkin, which was done accordingly. Thus he was
once more compelled to submit to their tyranny. The petition concluded
with stating, that the Earl, under pretence of the deeds thus
iniquitously obtained, had taken possession of the whole place and
living of Crossraguel, and enjoyed the profits thereof for three years.

The doom of the Regent and Council shows singularly the total
interruption of justice at this calamitous period, even in the most
clamant cases of oppression. The Council declined interference with the
course of the ordinary justice of the county, (which was completely
under the said Earl of Cassilis' control,) and only enacted, that he
should forbear molestation of the unfortunate Comendator, under the
surety of two thousand pounds Scots. The Earl was appointed also to keep
the peace towards the celebrated George Buchanan, who had a pension out
of the same Abbacy, to a similar extent, and under the like penalty.

The consequences are thus described by the Journalist already quoted.--

``The said Laird of Bargany perceiving that the ordiner justice could
neither help the oppressed, nor yet the afflicted, applied his mind to
the next remedy, and in the end, by his servants, took the house of
Denure, where the poor Abbot was kept prisoner. The bruit flew fra
Carrick to Galloway, and so suddenly assembled herd and hyre-man that
pertained to the band of the Kennedies; and so within a few hours was
the house of Denure environed again. The master of Cassilis was the
frackast {[}i.e.~the readiest or boldest{]} and would not stay, but in
his heat would lay fire to the dungeon, with no small boasting that all
enemies within the house should die.

``He was required and admonished by those that were within to be more
moderate, and not to hazard himself so foolishly. But no admonition
would help, till that the wind of an hacquebute blasted his shoulder,
and then ceased he from further pursuit in fury. The Laird of Bargany
had before purchest {[}obtained{]} of the authorities, letters, charging
all faithfull subjects to the King's Majesty, to assist him against that
cruel tyrant and mansworn traitor, the Earl of Cassilis; which letters,
with his private writings, he published, and shortly found sic
concurrence of Kyle and Cunynghame with his other friends, that the
Carrick company drew back fra the house: and so the other approached,
furnished the house with more men, delivered the said Mr Allan, and
carried him to Ayr, where, publicly at the market cross of the said
town, he declared how cruelly he was entreated, and how the murdered
King suffered not sic torment as he did, excepting only he escaped the
death: and, therefore, publickly did revoke all things that were done in
that extremity, and especially revoked the subscription of the three
writings, to wit, of a fyve yeir tack and nineteen year tack, and of a
charter of feu. And so the house remained, and remains (till this day,
the 7th of February, 1571,) in the custody of the said Laird of Bargany
and of his servants. And so cruelty was disappointed of proffeit
present, and shall be eternallie punished, unless he earnestly repent.
And this far for the cruelty committed, to give occasion unto others,
and to such as hate the monstrous dealing of degenerate nobility, to
look more diligently upon their behaviuours, and to paint them forth
unto the world, that they themselves may be ashamed of their own
beastliness, and that the world may be advertised and admonished to
abhor, detest, and avoid the company of all sic tyrants, who are not
worthy of the society of men, but ought to be sent suddenly to the
devil, with whom they must burn without end, for their contempt of God,
and cruelty committed against his creatures. Let Cassilis and his
brother be the first to be the example unto others. Amen.
Amen.''\footnote{Bannatyne's Journal.}

This extract has been somewhat amended or modernized in orthography, to
render it more intelligible to the general reader. I have to add, that
the Kennedies of Bargany, who interfered in behalf of the oppressed
Abbot, were themselves a younger branch of the Cassilis family, but held
different politics, and were powerful enough in this, and other
instances, to bid them defiance.

The ultimate issue of this affair does not appear; but as the house of
Cassilis are still in possession of the greater part of the feus and
leases which belonged to Crossraguel Abbey, it is probable the talons of
the King of Carrick were strong enough, in those disorderly times, to
retain the prey which they had so mercilessly fixed upon.

I may also add, that it appears by some papers in my possession, that
the officers or Country Keepers on the border, were accustomed to
torment their prisoners by binding them to the iron bars of their
chimneys, to extort confession.

\section*{Note to Chapter XXIX} \label{noteCXXIX}

Note F.--Heraldry

The author has been here upbraided with false heraldry, as having
charged metal upon metal. It should be remembered, however, that
heraldry had only its first rude origin during the crusades, and that
all the minutiae of its fantastic science were the work of time, and
introduced at a much later period. Those who think otherwise must
suppose that the Goddess of ``Armoirers'', like the Goddess of Arms,
sprung into the world completely equipped in all the gaudy trappings of
the department she presides over.

\noindent Additional Note

In corroboration of said note, it may be observed, that the arms, which
were assumed by Godfrey of Boulogne himself, after the conquest of
Jerusalem, was a cross counter patent cantoned with four little crosses
or, upon a field azure, displaying thus metal upon metal. The heralds
have tried to explain this undeniable fact in different modes--but Ferne
gallantly contends, that a prince of Godfrey's qualities should not be
bound by the ordinary rules. The Scottish Nisbet, and the same Ferne,
insist that the chiefs of the Crusade must have assigned to Godfrey this
extraordinary and unwonted coat-of-arms, in order to induce those who
should behold them to make enquiries; and hence give them the name of
``arma inquirenda''. But with reverence to these grave authorities, it
seems unlikely that the assembled princes of Europe should have adjudged
to Godfrey a coat armorial so much contrary to the general rule, if such
rule had then existed; at any rate, it proves that metal upon metal, now
accounted a solecism in heraldry, was admitted in other cases similar to
that in the text. See Ferne's ``Blazon of Gentrie'' p.~238. Edition
1586. Nisbet's ``Heraldry'', vol.~i. p.~113. Second Edition.

\section*{Note to Chapter XXXI} \label{noteCXXXI}

Note G.--Ulrica's Death song.

It will readily occur to the antiquary, that these verses are intended
to imitate the antique poetry of the Scalds--the minstrels of the old
Scandinavians--the race, as the Laureate so happily terms them,

\begin{verse}
``Stern to inflict, and stubborn to endure,\\
Who smiled in death.''\\!
\end{verse}

The poetry of the Anglo-Saxons, after their civilisation and conversion,
was of a different and softer character; but in the circumstances of
Ulrica, she may be not unnaturally supposed to return to the wild
strains which animated her forefathers during the time of Paganism and
untamed ferocity.

\section*{Note to Chapter XXXII} \label{noteCXXXII}

Note H.--Richard Coeur-de-Lion.

The interchange of a cuff with the jolly priest is not entirely out of
character with Richard I., if romances read him aright. In the very
curious romance on the subject of his adventures in the Holy Land, and
his return from thence, it is recorded how he exchanged a pugilistic
favour of this nature, while a prisoner in Germany. His opponent was the
son of his principal warder, and was so imprudent as to give the
challenge to this barter of buffets. The King stood forth like a true
man, and received a blow which staggered him. In requital, having
previously waxed his hand, a practice unknown, I believe, to the
gentlemen of the modern fancy, he returned the box on the ear with such
interest as to kill his antagonist on the spot.--See, in Ellis's
Specimens of English Romance, that of Coeur-de-Lion.

\section*{Note to Chapter XXXIII} \label{noteCXXXIII}

Note I.--Hedge-Priests.

It is curious to observe, that in every state of society, some sort of
ghostly consolation is provided for the members of the community, though
assembled for purposes diametrically opposite to religion. A gang of
beggars have their Patrico, and the banditti of the Apennines have among
them persons acting as monks and priests, by whom they are confessed,
and who perform mass before them. Unquestionably, such reverend persons,
in such a society, must accommodate their manners and their morals to
the community in which they live; and if they can occasionally obtain a
degree of reverence for their supposed spiritual gifts, are, on most
occasions, loaded with unmerciful ridicule, as possessing a character
inconsistent with all around them.

Hence the fighting parson in the old play of Sir John Oldcastle, and the
famous friar of Robin Hood's band. Nor were such characters ideal. There
exists a monition of the Bishop of Durham against irregular churchmen of
this class, who associated themselves with Border robbers, and
desecrated the holiest offices of the priestly function, by celebrating
them for the benefit of thieves, robbers, and murderers, amongst ruins
and in caverns of the earth, without regard to canonical form, and with
torn and dirty attire, and maimed rites, altogether improper for the
occasion.

\section*{Note to Chapter XLI} \label{noteCXLI}

Note J.--Castle of Coningsburgh.

When I last saw this interesting ruin of ancient days, one of the very
few remaining examples of Saxon fortification, I was strongly impressed
with the desire of tracing out a sort of theory on the subject, which,
from some recent acquaintance with the architecture of the ancient
Scandinavians, seemed to me peculiarly interesting. I was, however,
obliged by circumstances to proceed on my journey, without leisure to
take more than a transient view of Coningsburgh. Yet the idea dwells so
strongly in my mind, that I feel considerably tempted to write a page or
two in detailing at least the outline of my hypothesis, leaving better
antiquaries to correct or refute conclusions which are perhaps too
hastily drawn.

Those who have visited the Zetland Islands, are familiar with the
description of castles called by the inhabitants Burghs; and by the
Highlanders--for they are also to be found both in the Western Isles and
on the mainland--Duns. Pennant has engraved a view of the famous
Dun-Dornadilla in Glenelg; and there are many others, all of them built
after a peculiar mode of architecture, which argues a people in the most
primitive state of society. The most perfect specimen is that upon the
island of Mousa, near to the mainland of Zetland, which is probably in
the same state as when inhabited.

It is a single round tower, the wall curving in slightly, and then
turning outward again in the form of a dice-box, so that the defenders
on the top might the better protect the base. It is formed of rough
stones, selected with care, and laid in courses or circles, with much
compactness, but without cement of any kind. The tower has never, to
appearance, had roofing of any sort; a fire was made in the centre of
the space which it encloses, and originally the building was probably
little more than a wall drawn as a sort of screen around the great
council fire of the tribe. But, although the means or ingenuity of the
builders did not extend so far as to provide a roof, they supplied the
want by constructing apartments in the interior of the walls of the
tower itself. The circumvallation formed a double enclosure, the inner
side of which was, in fact, two feet or three feet distant from the
other, and connected by a concentric range of long flat stones, thus
forming a series of concentric rings or stories of various heights,
rising to the top of the tower. Each of these stories or galleries has
four windows, facing directly to the points of the compass, and rising
of course regularly above each other. These four perpendicular ranges of
windows admitted air, and, the fire being kindled, heat, or smoke at
least, to each of the galleries. The access from gallery to gallery is
equally primitive. A path, on the principle of an inclined plane, turns
round and round the building like a screw, and gives access to the
different stories, intersecting each of them in its turn, and thus
gradually rising to the top of the wall of the tower. On the outside
there are no windows; and I may add, that an enclosure of a square, or
sometimes a round form, gave the inhabitants of the Burgh an opportunity
to secure any sheep or cattle which they might possess.

Such is the general architecture of that very early period when the
Northmen swept the seas, and brought to their rude houses, such as I
have described them, the plunder of polished nations. In Zetland there
are several scores of these Burghs, occupying in every case, capes,
headlands, islets, and similar places of advantage singularly well
chosen. I remember the remains of one upon an island in a small lake
near Lerwick, which at high tide communicates with the sea, the access
to which is very ingenious, by means of a causeway or dike, about three
or four inches under the surface of the water. This causeway makes a
sharp angle in its approach to the Burgh. The inhabitants, doubtless,
were well acquainted with this, but strangers, who might approach in a
hostile manner, and were ignorant of the curve of the causeway, would
probably plunge into the lake, which is six or seven feet in depth at
the least. This must have been the device of some Vauban or Cohorn of
those early times.

The style of these buildings evinces that the architect possessed
neither the art of using lime or cement of any kind, nor the skill to
throw an arch, construct a roof, or erect a stair; and yet, with all
this ignorance, showed great ingenuity in selecting the situation of
Burghs, and regulating the access to them, as well as neatness and
regularity in the erection, since the buildings themselves show a style
of advance in the arts scarcely consistent with the ignorance of so many
of the principal branches of architectural knowledge.

I have always thought, that one of the most curious and valuable objects
of antiquaries has been to trace the progress of society, by the efforts
made in early ages to improve the rudeness of their first expedients,
until they either approach excellence, or, as is more frequently the
case, are supplied by new and fundamental discoveries, which supersede
both the earlier and ruder system, and the improvements which have been
ingrafted upon it. For example, if we conceive the recent discovery of
gas to be so much improved and adapted to domestic use, as to supersede
all other modes of producing domestic light; we can already suppose,
some centuries afterwards, the heads of a whole Society of Antiquaries
half turned by the discovery of a pair of patent snuffers, and by the
learned theories which would be brought forward to account for the form
and purpose of so singular an implement.

Following some such principle, I am inclined to regard the singular
Castle of Coningsburgh--I mean the Saxon part of it--as a step in
advance from the rude architecture, if it deserves the name, which must
have been common to the Saxons as to other Northmen. The builders had
attained the art of using cement, and of roofing a building,--great
improvements on the original Burgh. But in the round keep, a shape only
seen in the most ancient castles--the chambers excavated in the
thickness of the walls and buttresses--the difficulty by which access is
gained from one story to those above it, Coningsburgh still retains the
simplicity of its origin, and shows by what slow degrees man proceeded
from occupying such rude and inconvenient lodgings, as were afforded by
the galleries of the Castle of Mousa, to the more splendid
accommodations of the Norman castles, with all their stern and Gothic
graces.

I am ignorant if these remarks are new, or if they will be confirmed by
closer examination; but I think, that, on a hasty observation,
Coningsburgh offers means of curious study to those who may wish to
trace the history of architecture back to the times preceding the Norman
Conquest.

It would be highly desirable that a cork model should be taken of the
Castle of Mousa, as it cannot be well understood by a plan.

The Castle of Coningsburgh is thus described:--

``The castle is large, the outer walls standing on a pleasant ascent
from the river, but much overtopt by a high hill, on which the town
stands, situated at the head of a rich and magnificent vale, formed by
an amphitheatre of woody hills, in which flows the gentle Don. Near the
castle is a barrow, said to be Hengist's tomb. The entrance is flanked
to the left by a round tower, with a sloping base, and there are several
similar in the outer wall the entrance has piers of a gate, and on the
east side the ditch and bank are double and very steep. On the top of
the churchyard wall is a tombstone, on which are cut in high relief, two
ravens, or such-like birds. On the south side of the churchyard lies an
ancient stone, ridged like a coffin, on which is carved a man on
horseback; and another man with a shield encountering a vast winged
serpent, and a man bearing a shield behind him. It was probably one of
the rude crosses not uncommon in churchyards in this county. See it
engraved on the plate of crosses for this volume, plate 14. fig.~1. The
name of Coningsburgh, by which this castle goes in the old editions of
the Britannia, would lead one to suppose it the residence of the Saxon
kings. It afterwards belonged to King Harold. The Conqueror bestowed it
on William de Warren, with all its privileges and jurisdiction, which
are said to have extended over twenty-eight towns. At the corner of the
area, which is of an irregular form, stands the great tower, or keep,
placed on a small hill of its own dimensions, on which lies six vast
projecting buttresses, ascending in a steep direction to prop and
support the building, and continued upwards up the side as turrets. The
tower within forms a complete circle, twenty-one feet in diameter, the
walls fourteen feet thick. The ascent into the tower is by an exceeding
deep flight of steep steps, four feet and a half wide, on the south side
leading to a low doorway, over which is a circular arch crossed by a
great transom stone. Within this door is the staircase which ascends
straight through the thickness of the wall, not communicating with the
room on the first floor, in whose centre is the opening to the dungeon.
Neither of these lower rooms is lighted except from a hole in the floor
of the third story; the room in which, as well as in that above it, is
finished with compact smooth stonework, both having chimney-pieces, with
an arch resting on triple clustered pillars. In the third story, or
guard-chamber, is a small recess with a loop-hole, probably a
bedchamber, and in that floor above a niche for a saint or holy-water
pot. Mr.~King imagines this a Saxon castle of the first ages of the
Heptarchy. Mr.~Watson thus describes it. From the first floor to the
second story, (third from the ground,) is a way by a stair in the wall
five feet wide. The next staircase is approached by a ladder, and ends
at the fourth story from the ground. Two yards from the door, at the
head of this stair, is an opening nearly east, accessible by treading on
the ledge of the wall, which diminishes eight inches each story; and
this last opening leads into a room or chapel ten feet by twelve, and
fifteen or sixteen high, arched with free-stone, and supported by small
circular columns of the same, the capitals and arches Saxon. It has an
east window, and on each side in the wall, about four feet from the
ground, a stone basin with a hole and iron pipe to convey the water into
or through the wall. This chapel is one of the buttresses, but no sign
of it without, for even the window, though large within, is only a long
narrow loop-hole, scarcely to be seen without. On the left side of this
chapel is a small oratory, eight by six in the thickness of the wall,
with a niche in the wall, and enlightened by a like loop-hole. The
fourth stair from the ground, ten feet west from the chapel door, leads
to the top of the tower through the thickness of the wall, which at top
is but three yards. Each story is about fifteen feet high, so that the
tower will be seventy-five feet from the ground. The inside forms a
circle, whose diameter may be about twelve feet. The well at the bottom
of the dungeon is piled with stones.''--Gough's ``Edition Of Camden's
Britannia''. Second Edition, vol.~iii. p.~267.
