\section*{THE CORVIDÆ OR CROW FAMILY}

\lettrine{T}{his} family, which is well represented in the Himalayas, includes
the true crows, with their allies, the choughs, pies, jays, and tits.

The common Indian house-crow (\textit{Corvus splendens}), with which every
Anglo-Indian is only too familiar, loveth not great altitudes, hence
does not occur in any of the higher hill stations. Almora is the one
place in the hills where he appears to be common. There he displays
all the shameless impudence of his brethren in the plains.

The common crow of the Himalayas is the large all-black species which
is known as the Indian corby or jungle crow (\textit{C. macrorhynchus}).
Unlike its grey-necked cousin, this bird is not a public nuisance;
nevertheless it occasionally renders itself objectionable by
carrying off a chicken or a tame pigeon. In May or June it constructs,
high up in a tree, a rough nest, which is usually well concealed by
the thick foliage. The nest is a shallow cup or platform in the midst
of which is a depression, lined with grass and hair. Horse-hair is
used in preference to other kinds of hair; if this be not available
crows will use human hair, or hair plucked from off the backs of cattle.
Those who put out skins to dry are warned that nesting crows are apt
to damage them seriously. Three or four eggs are laid. These are dull
green, speckled with brown. Crows affect great secrecy regarding
their nests. If a pair think that their nursery is being looked at
by a human being, they show their displeasure by swearing as only
crows can, and by tearing pieces of moss off the branch of some tree
and dropping these on the offender's head!

Two species of chough, the red-billed (\textit{Graculus eremita}), which
is identical with the European form, and the yellow-billed chough
(\textit{Pyrrhocorax alpinus}), are found in the Himalayas; but he who would
see them must either ascend nearly to the snow-line or remain on in
the hills during the winter.

Blue-magpies are truly magnificent birds, being in appearance not
unlike small pheasants. Two species grace the Himalayas: the
red-billed (\textit{Urocissa occipitalis}) and the yellow-billed
blue-magpie (\textit{U. flavirostris}). These are distinguishable one from
the other mainly by the colour of the beak. A blue-magpie is a bird
over 2 feet in length, of which the fine tail accounts for
three-fourths. The head, neck, and breast are black, and the remainder
of the plumage is a beautiful blue with handsome white markings. It
is quite unnecessary to describe the blue-magpie in detail. It is
impossible to mistake it. Even a blind man cannot fail to notice it
because of its loud ringing call. East of Simla the red-billed species
is by far the commoner, while to the west the yellow-billed form rules
the roost. The vernacular names for the blue-magpie are \textit{Nilkhant}
at Mussoorie and \textit{Dig-dall} at Simla.

The Himalayan tree-pie (\textit{Dendrocitta himalayensis}), although a fine
bird, looks mean in comparison with his blue cousins. This species
is like a dull edition of the tree-pie of the plains. It is dressed
like a quaker. It is easily recognised when on the wing. Its flight
is very characteristic, consisting of a few rapid flaps of the pinions
followed by a sail on outstretched wings. The median pair of tail
feathers is much longer than the others, the pair next to the middle
one is the second longest, and the outer one shortest of all. Thus
the tail, when expanded during flight, has a curious appearance.

We now come to the jays. That brilliant study in light and dark blue,
so common in the plains, which we call the blue-jay, does not occur
in the Himalayas; nor is it a jay at all: its proper name is the Indian
roller (\textit{Coracias indica}). It is in no way connected with the jay
tribe, being not even a passerine bird. We know this because of the
arrangement of its deep plantar tendons, because its palate is
desmognathous instead of ægithognathous, because--but I think I will
not proceed further with these reasons; if I do, this article will
resemble a letter written by the conscientious undergraduate who used
to copy into each of his epistles to his mother, a page of \textit{A Complete
Guide to the Town of Cambridge}. The fond mother doubtless found her
son's letters very instructive, but they were not exactly what she
wanted. Let it suffice that the familiar bird with wings of two shades
of blue is not a jay, nor even one of the Corviniæ, but a blood relation
of the kingfishers and bee-eaters.

Two true jays, however, are common in the Western Himalayas. These
are known to science as the Himalayan jay (\textit{Garrulus bispecularis})
and the black-throated jay (\textit{G. lanceolatus}). The former is a
fawn-coloured bird, with a black moustachial streak. As birds do not
usually indulge in moustaches, this streak renders the bird an easy
one to identify. The tail is black, and the wing has the characteristic
blue band with narrow black cross-bars. This species goes about in
large noisy flocks. Once at Naini Tal I came upon a flock which cannot
have numbered fewer than forty individuals.

The handsome black-throated jay is a bird that must be familiar to
every one who visits a Himalayan hill station with his eyes open.
Nevertheless no one seems to have taken the trouble to write about
it. Those who have compiled lists of birds usually dismiss it in their
notes with such adjectives as ``abundant,'' and ``very common.'' It is
remarkable that many popular writers should have discoursed upon the
feathered folk of the plains, while few have devoted themselves to
the interesting birds of the hills. There seem to be two reasons for
this neglect of the latter. Firstly, it is only the favoured few to
whom it is given to spend more than ten days at a time in the cool
heights; most of us have to toil in the hot plains. Secondly, the
thick foliage of the mountain-side makes bird-watching a somewhat
difficult operation. The observer frequently catches sight of an
interesting-looking bird, only to see it disappear among the foliage
before he has had time even to identify it.

The black-throated jay is a handsome bird, more striking in appearance
even than the jay of England (\textit{G. glandarius}). Its crested head is
black. Its back is a beautiful French grey, its wings are black and
white with a bar of the peculiar shade of blue which is characteristic
of the jay family and so rarely seen in nature or art. Across this
blue bar run thin black transverse lines. The tail is of the same
blue with similar black cross-bars, and each feather is tipped with
white. The throat is black, with short white lines on it. The legs
are pinkish slaty, and the bill is slate coloured in some individuals,
and almost white in others. The size of this jay is the same as that
of our familiar English one. Black-throated jays go about in flocks.
This is a characteristic of a great many Himalayan birds. Probably
the majority of the common birds of these mountains lead a sociable
existence, like that of the ``seven sisters'' of the plains. A man may
walk for half-an-hour through a Himalayan wood without seeing a bird
or hearing any bird-sound save the distant scream of a kite or the
raucous voice of the black crow; then suddenly he comes upon quite
a congregation of birds, a flock of a hundred or more noisy
laughing-thrushes, or numbers of cheeping white-eyes and tits, or
it may be a flock of rowdy black bulbuls. All the birds of the wood
seem to be collected in one place. This flocking of the birds in the
hills must, I think, be accounted for by the fact that birds are by
nature sociable creatures, and that food is particularly abundant.
In a dense wood every tree offers either insect or vegetable food,
so that a large number of birds can live in company without fear of
starving each other out. In the plains food is less abundant, hence
most birds that dwell there are able to gratify their fondness for
each other's society only at roosting time; during the day they are
obliged to separate, in order to find the wherewithal to feed upon.

Like all sociable birds, the black-throated jay is very noisy. Birds
have a language of a kind, a language composed entirely of
interjections, a language in which only the simplest emotions--fear,
joy, hunger, and maternal care--can be expressed. Now, when a
considerable flock of birds is wandering through a dense forest, it
is obvious that the individuals which compose it would be very liable
to lose touch with one another had they no means of informing one
another of their whereabouts. The result is that such a means has
been developed. Every bird, whose habit it is to go about in company,
has the habit of continually uttering some kind of call or cry. It
probably does this unconsciously, without being aware that it is
making any sound.

In Madras a white-headed babbler nestling was once brought to me.
I took charge of it and fed it, and noticed that when it was not asleep
it kept up a continuous cheeping all day long, even when it was eating,
although it had no companion. The habit of continually uttering its
note was inherited. When the flock is stationary the note is a
comparatively low one; but when an individual makes up its mind to
fly any distance, say ten or a dozen yards, it gives vent to a louder
call, so as to inform its companions that it is moving. This sound
seems to induce others to follow its lead. This is especially
noticeable in the case of the white-throated laughing-thrush. I have
seen one of these birds fly to a branch in a tree, uttering its curious
call, and then hop on to another branch in the same tree. Scarcely
has it left the first branch when a second laughing-thrush flies to
it; then a fourth, a fifth, and so on; so that the birds look as though
they might be playing ``Follow the man from Cook's.'' The black-throated
jay is noisy even for a sociable bird. The sound which it seems to
produce more often than any other is very like the harsh anger-cry
of the common myna. Many Himalayan birds have rather discordant notes,
and in this respect these mountains do not compare favourably with
the Nilgiris, where the blithe notes of the bulbuls are very pleasing
to the ear.

Jays are by nature bold birds. They are inclined to be timid in England,
because they are so much persecuted by the game-keeper. In the
Himalayas they are as bold as the crow. It is not uncommon to see
two or three jays hopping about outside a kitchen picking up the scraps
pitched out by the cook. Sometimes two jays make a dash at the same
morsel. Then a tiff ensues, but it is mostly made up of menacing
screeches. One bird bears away the coveted morsel, swearing lustily,
and the unsuccessful claimant lets him go in peace. When a jay comes
upon a morsel of food too large to be swallowed whole, it flies with
it to a tree and holds it under one foot and tears it up with its
beak. This is a characteristically corvine habit. The black-throated
jay is an exceedingly restless bird; it is always on the move. Like
its English cousin, it is not a bird of very powerful flight. As
Gilbert White says: ``Magpies and jays flutter with powerless wings,
and make no despatch.'' In the Himalayas there is no necessity for
it to make much despatch; it rarely has to cover any distance on the
wing. When it does fly a dozen yards or so, its passage is marked
by much noisy flapping of the pinions.

The nutcrackers can scarcely be numbered among the common birds, but
are sometimes seen in our hill stations, and, such is the ``cussedness''
of birds that if I omit to notice the nutcrackers several are certain
to show themselves to many of those who read these lines. A
chocolate-brown bird, bigger than a crow, and spotted and barred with
white all over, can be nothing other than one of the Himalayan
nutcrackers. It may be the Himalayan species (\textit{Nucifraga hemispila}),
or the larger spotted nutcracker (\textit{N. multipunctata}).

The members of the crow family which I have attempted to describe
above are all large birds, birds bigger than a crow. It now behoves
us to consider the smaller members of the corvine clan.

The tits form a sub-family of the crows. Now at first sight the crow
and the tit seem to have but little in common. However, close
inspection, whether by the anatomist or the naturalist, reveals the
mark of the corvidæ in the tits. First, there is the habit of holding
food under the foot while it is being devoured. Then there is the
aggressiveness of the tits. This is Lloyd-Georgian or even Winstonian
in its magnitude. ``Tits,'' writes Jerdon, ``are excessively bold and
even ferocious, the larger ones occasionally destroying young and
sickly birds, both in a wild state and in confinement.''

Many species of tit dwell in the Himalayas. To describe them all would
bewilder the reader; I will, therefore, content myself with brief
descriptions of four species, each of which is to be seen daily in
every hill station of the Western Himalayas.

The green-backed tit (\textit{Parus monticola}) is a glorified edition of
our English great tit. It is a bird considerably smaller than a
sparrow.

The cheeks are white, the rest of the head is black, as are the breast
and a characteristic line running along the abdomen. The back is
greenish yellow, the lower parts are deep yellow. The wings are black
with two white bars, the tail is black tipped with white. This is
one of the commonest birds in most hill stations.

Like the sparrow, it is ever ready to rear up its brood in a hole
in the wall of a house. Any kind of a hole will do, provided the aperture
is too small to admit of the entrance of birds larger than itself.

The nesting operations of a pair of green-backed tits form the subject
of a separate essay.

Another tit much in evidence is the yellow-cheeked tit, \textit{Machlolophus
xanthogenys}. I apologise for its scientific name. Take a
green-backed tit, paint its cheeks bright yellow, and give it a black
crest tipped with yellow, and you will have transformed him into a
yellow-cheeked tit.

There remain to be described two pigmy tits. The first of these is
that feathered exquisite, the red-headed tit (\textit{Ægithaliscus
erythrocephalus}). I will not again apologise for the name; it must
suffice that the average ornithologist is never happy unless he be
either saddling a small bird with a big name or altering the
denomination of some unfortunate fowl. This fussy little mite is not
so long as a man's thumb. It is crestless; the spot where the crest
ought to be is chestnut red. The remainder of the upper plumage is
bluish grey, while the lower plumage is the colour of rust. The black
face is set off by a white eyebrow. Last, but not least, of our common
tits is the crested black tit (\textit{Lophophanes melanopterus}). The
crested head and breast of this midget are black. The cheeks and nape
are white, while the rest of the upper plumage is iron grey.

There is yet another tit of which mention must be made, because he
is the common tit of Almora. The climate of Almora is so much milder
than that of other hill stations that its birds are intermediate
between those of the hills and the plains. The Indian grey tit (\textit{Parus
atriceps}) is a bird of wide distribution. It is the common tit of
the Nilgiris, is found in many of the better-wooded parts of the plains,
and ascends the Himalayas up to 6000 feet. It is a grey bird with
the head, neck, breast, and abdominal line black. The cheeks are white.
It is less gregarious than the other tits. Its notes are harsh and
varied, being usually a \textit{ti-ti-chee} or \textit{pretty-pretty}.

I have not noticed this species at either Mussoorie or Naini Tal,
but, as I have stated, it is common at Almora.

As has been mentioned above, tits usually go about in flocks. It is
no uncommon thing for a flock to contain all of the four species of
tit just described, a number of white-eyes, some nuthatches, warblers,
tree-creepers, a woodpecker or two, and possibly some sibias and
laughing-thrushes.
