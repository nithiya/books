\chapter{THE HABITAT OF HIMALAYAN BIRDS}

\lettrine{H}{imalayan} birds inhabit what is perhaps the most wonderful tract of
country in the world. The Himalayas are not so much a chain of
mountains as a mountainous country, some eighty miles broad and
several hundred long--a country composed entirely of mountains and
valleys with no large plains or broad plateaux.

There is a saying of an ancient Sanskrit poet which, being translated
into English, runs: ``In a hundred ages of the gods I could not tell
you of the glories of Himachal.'' This every writer on things Himalayan
contrives to drag into his composition. Some begin with the quotation,
while others reserve it for the last, and make it do duty for the
epigram which stylists assure us should terminate every essay.

Some there are who quote the Indian sage only to mock him. Such assert
that the beauties of the Himalayas have been greatly exaggerated--that,
as regards grandeur, their scenery compares unfavourably with that of
the Andes, while their beauty is surpassed by that of the Alps. Not
having seen the Andes, I am unable to criticise the assertion
regarding the grandeur of the Himalayas, but I find it difficult to
imagine anything finer than their scenery.

As regards beauty, the Himalayas at their best surpass the Alps,
because they exhibit far more variety, and present everything on a
grander scale.

The Himalayas are a kind of Dr. Jekyll and Mr. Hyde. They have two
faces--the fair and the plain. In May they are at their worst. Those
of the hillsides which are not afforested are brown, arid, and
desolate, and the valleys, in addition to being unpleasantly hot,
are dry and dusty. The foliage of the trees lacks freshness, and
everywhere there is a remarkable absence of water, save in the valleys
through which the rivers flow. On the other hand, September is the
month in which the Himalayas attain perfection or something
approaching it. The eye is refreshed by the bright emerald garment
which the hills have newly donned. The foliage is green and luxuriant.
Waterfalls, cascades, mighty torrents and rivulets abound. Himachal
has been converted into fairyland by the monsoon rains.

A remarkable feature of the Himalayas is the abruptness with which
they rise from the plains in most places. In some parts there are
low foothills; but speaking generally the mountains that rise from
the plain attain a height of 4000 or 5000 feet.

It is difficult for any person who has not passed from the plains
of India to the Himalayas to realise fully the vast difference between
the two countries and the dramatic suddenness with which the change
takes place.

The plains are as flat as the proverbial pancake--a dead monotony
of cultivated alluvium, square mile upon square mile of wheat, rice,
vetch, sugar-cane, and other crops, amidst which mango groves, bamboo
clumps, palms, and hamlets are scattered promiscuously. In some
places the hills rise sheer from this, in others they are separated
from the alluvial plains by belts of country known as the Tarai and
Bhabar. The Tarai is low-lying, marshy land covered with tall,
feathery grass, beautifully monotonous. This is succeeded by a
stretch of gently-rising ground, 10 or 20 miles in breadth, known
as the Bhabar--a strip of forest composed mainly of tall evergreen
\textit{sal} trees (\textit{Shorea robusta}). These trees grow so close together
that the forest is difficult to penetrate, especially after the rains,
when the undergrowth is dense and rank. Very beautiful is the Bhabar,
and very stimulating to the imagination. One writer speaks of it as
``a jungle rhapsody, an extravagant, impossible botanical \textit{tour de
force}, intensely modern in its Titanic, incoherent magnificence.''
It is the home of the elephant, the tiger, the panther, the wild boar,
several species of deer, and of many strange and beautiful birds.

Whether from the flat plains or the gently-sloping Bhabar, the
mountains rise with startling suddenness.

The flora and fauna of the Himalayas differ from those of the
neighbouring plains as greatly as the trees and animals of England
differ from those of Africa.

Of the common trees of the plains of India--the \textit{nim}, mango, babul,
tamarind, shesham, palm, and plantain--not one is to be found growing
on the hills. The lower slopes are covered with \textit{sal} trees like the
Bhabar. These cease to grow at elevations of 3000 feet above the
sea-level, and, higher up, every rise of 1000 feet means a
considerable change in the flora. Above the \textit{sal} belt come several
species of tropical evergreen trees, among the stems and branches
of which great creepers entangle themselves in fantastic figures.
At elevations of 4000 feet the long-leaved pine (\textit{Pinus longifolia})
appears. From 5000 to 10,000 feet, several species of evergreen oaks
abound. Above 6000 feet are to be seen the rhododendron, the deodar
and other hill cypresses, and the beautiful horse-chestnut. On the
lower slopes the undergrowth is composed largely of begonias and
berberry. Higher up maidenhair and other ferns abound, and the trunks
of the oaks and rhododendrons are festooned with hanging moss.

Between elevations of 10,000 and 12,000 feet the silver fir is the
commonest tree. Above 12,000 feet the firs become stunted and dwarfed,
on account of the low temperatures that prevail, and juniper and birch
are the characteristic trees.

There are spots in the Himalayas, at heights varying from 10,000 to
12,000 feet, where wild raspberries grow, and the yellow colt's-foot,
the dandelion, the blue gentian, the Michaelmas daisy, the purple
columbine, the centauria, the anemone, and the edelweiss occur in
profusion. Orchids grow in large numbers in most parts of the
Himalayas.

Every hillside is not covered with foliage. Many are rugged and bare.
Some of these are too precipitous to sustain vegetation, others are
masses of quartz and granite. On the hillsides most exposed to the
wind, only grass and small shrubs are able to obtain a foothold.

``On the vast ridges of elevated mountain masses,'' writes Weber in
\textit{The Forests of Upper India}, ``which constitute the Himalayas are
found different regions of distinct character. The loftiest peaks
of the snowy range abutting on the great plateaux of Central Asia
and Tibet run like a great belt across the globe, falling towards
the south-west to the plains of India. Between the summit and the
plains, a distance of 60 to 70 miles, there are higher, middle, and
lower ranges, so cut up by deep and winding valleys and river-courses,
that no labyrinth could be found more confusing or difficult to
unravel. There is nowhere any tableland, as at the Cape or in Colorado,
with horizontal strata of rock cut down by water into valleys or cañons.
The strata seem, on the contrary, to have been shoved up and crumpled
in all directions by some powerful shrinkage of the earth's crust,
due perhaps to cooling; and the result is such a jumble of contorted
rock masses, that it looks as if some great castle had been blown
up by dynamite and its walls hurled in all directions. The great
central masses, however, consist generally of crystalline granite,
gneiss, and quartz rock, protruding from the bowels of the earth and
shoving up the stratified envelope of rocks nearly 6 miles above
sea-level.... The higher you get up ... the rougher and more difficult
becomes the climbing; the valleys are deeper and more cut into ravines,
the rocks more fantastically and rudely torn asunder, and the very
vitals of the earth exposed; while the heights above tower to the
skies. The torrents rushing from under the glaciers which flow from
the snow-clad summits roar and foam, eating their way ever into the
misty gorges.''

Those who have not visited the Himalayas may perhaps best obtain an
idea of the nature of the country from a brief description of that
traversed by a path leading from the plain to the snowy range. Let
us take the path from Kathgodam, the terminus of the Rohilkhand and
Kumaun railway, to the Pindari glacier.

For the first two miles the journey is along the cart-road to Naini
Tal, on the right bank of the Gola river.

At Ranibagh the pilgrim to the Pindari glacier leaves the cart-road
and follows a bridle-path which, having crossed the Gola by a
suspension bridge, mounts the steep hill on the left bank. Skirting
this hill on its upward course, the road reaches the far side, which
slopes down to the Barakheri stream. A fairly steep ascent of 5 miles
through well-wooded country brings the traveller to Bhim Tal, a lake
4500 feet above the level of the sea. This lake, of which the area
is about 150 acres, is one of the largest of a series of lakes formed
by the flow of mountain streams into cup-like valleys. The path skirts
the lake and then ascends the Gagar range, which attains a height
of over 7000 feet. From the pass over this range a very fine view
is obtainable. To the north the snowy range stretches, and between
it and the pass lie 60 miles of mountain and valley. To the south
are to be seen Bhim Tal, Sat Tal, and other lakes, nestling in the
outer ranges, and, beyond the hills, the vast expanse of the plains.

The Gagar range is well wooded. The majority of the trees are
rhododendrons: these, when they put forth their blossoms in spring,
display a mass of crimson colouring. From the Gagar pass the road
descends for some 3 miles through forest to the valley of the Ramganga.
For about a mile the path follows the left bank of this small stream;
it then crosses it by a suspension bridge, and forthwith begins to
mount gradually the bare rocky Pathargarhi mountain. On the mountain
side, a few hundred feet above the Ramganga, is a village of three
score double-storeyed houses. These are very picturesque. Their
white walls are set off by dark brown woodwork. But alas they are
as whited sepulchres. It is only from a distance that they are
picturesque. They are typical abodes of the hill folk.

From the Pathargarhi pass the path makes a steep descent down a
well-wooded mountain-side to the Deodar stream. After crossing this
by a stone bridge, the path continues its switch-back course upwards
on a wooded hillside to the Laldana Binaik pass, whence it descends
gradually for 6 miles, through first rhododendron then pine forest
to the Sual river. This river is crossed by a suspension bridge. From
the Sual the path makes an ascent of 3 miles on a rocky hillside to
Almora, which is 36 miles from Kathgodam.

Almora used to be a Gurkha stronghold, and is now a charming little
hill station situated some 5300 feet above the sea-level.

The town and the civil and military station are built on a
saddle-backed ridge which is about 2 miles in length.

The Almora hill was almost completely denuded of trees by the Gurkhas,
but the ridge has since become well wooded. Deodar, pine, \textit{tun},
horse-chestnut, and alder trees are plentiful, and throughout the
cantonment grows a spiræa hedge.

The avifauna of Almora is very interesting, consisting as it does
of a strange mixture of hills and plains birds. Among the latter the
most prominent are the grey-necked crow, the koel, the myna, the
king-crow and the magpie-robin. In the spring paradise flycatchers
are very abundant.

From Almora the road to the snowy range runs over an almost treeless
rocky mountain called Kalimat, which rises to a height of 6500 feet.
From Kalimat the road descends to Takula--16 miles from Almora. Then
there is a further descent of 11 miles to Bageswar--a small town
situated on the Sarju river. The inhabitants of Bageswar lead a sleepy
existence for 360 days in the year, awakening for a short time in
January, when a big fair is held, to which flock men of Dhanpur,
Thibetans, Bhotias, Nepalese, Garwalis, and Kumaunis. These bring
wool, borax, and skins, which they exchange for the produce of the
plains.

From Bageswar the Pindari road is almost level for 22 miles, and runs
alongside the Sarju. At first the valley is wide and well cultivated.
Here and there are studded villages, of which the houses are roofed
with thatching composed of pine needles.

At a place about 16 miles above Bageswar the valley of the Sarju
suddenly contracts into a gorge with precipitous cliffs.

The scenery here is superb. The path passes through a shady glade
in the midst of which rushes the roaring, foaming river. The trunks
and larger branches of the trees are covered with ferns and hanging
moss. The landscape might well be the original for a phase of a
transformation scene at a pantomime. In the midst of this glade the
stream is crossed by a wooden bridge.

At a spot 2 miles above this the path, leaving the Sarju, takes a
sharp turn to the left, and begins a steep ascent of 5 miles up the
Dhakuri mountain. The base of this hill is well wooded. Higher up
the trees are less numerous. On the ridge the rhododendron and oak
forest alternates with large patches of grassland, on which wild
raspberries and brightly-coloured alpine flowers grow.

From the summit of the Dhakuri mountain a magnificent panorama
delights the eye. To the north is a deep valley, above which the
snow-clad mountains rise almost precipitously. Towering above the
observer are the peaks of the highest mountains in British territory.
The peaks and 14,000 feet of the slopes are covered with snow. Below
the snow is a series of glaciers: these are succeeded by rocks, grass,
and stunted vegetation until the tree-line is reached.

To the south lies the world displayed. Near at hand are 50 miles of
rugged mountainous country, and beyond the apparently limitless
plains. On a clear day it is said to be possible to distinguish the
minarets of Delhi, 300 miles away. In the early morning, when the
clouds still hover in the valleys, one seems to gaze upon a white
billowy sea studded with rocky islets.

From the Dhakuri pass the path descends about 2000 feet, and then
follows the valley of the Pindari river. The scenery here is
magnificent. Unlike that of the Sarju, this valley is narrow. It is
not much cultivated; amaranthus is almost the only crop grown. The
villages are few and the huts which constitute them are rudely
constructed. The cliffs are very high, and rise almost
perpendicularly, like giant walls, so that the numerous feeders of
the river take the form of cascades, in many of which the water falls
without interruption for a distance of over 1000 feet.

The Kuphini river joins the Pindar 8 miles from its source. Beyond
the junction the path to the glacier crosses to the left bank of the
Pindar, and then the ascent becomes steep. During the ascent the
character of the flora changes. Trees become fewer and flowers more
numerous; yellow colt's-foot, dandelions, gentians, Michaelmas
daisies, columbines, centaurias, anemones, and edelweiss grow in
profusion. Choughs, monal pheasants, and snow-pigeons are the
characteristic birds of this region.

Thus the birds of the Himalayas inhabit a country in every respect
unlike the plains of India. They dwell in a different environment,
are subjected to a different climate, and feed upon different food.
It is therefore not surprising that the two avifaunas should exhibit
great divergence. Nevertheless few people who have not actually been
in both localities are able to realise the startlingly abrupt
transformation of the bird-fauna seen by one who passes from the
plains to the hills.

The 5-mile journey from Rajpur to Mussoorie transports the traveller
from one bird-realm to another.

The caw of the house-crow is replaced by the deeper note of the corby.
Instead of the crescendo shriek of the koel, the pleasing double note
of the European cuckoo meets the ear. For the eternal \textit{coo-coo-coo-coo}
of the little brown dove, the melodious \textit{kokla-kokla} of the hill
green-pigeon is substituted. The harsh cries of the rose-ringed
paroquets give place to the softer call of the slaty-headed species.
The monotonous \textit{tonk-tonk-tonk} of the coppersmith and the
\textit{kutur-kutur-kutur} of the green barbet are no more heard; in their
stead the curious calls of the great Himalayan barbet resound among
the hills. The dissonant voices of the seven sisters no longer issue
from the thicket; their place is taken by the weird but less unpleasant
calls of the Himalayan streaked laughing-thrushes. Even the sounds of
the night are different. The chuckles and cackles of the spotted owlets
no longer fill the welkin; the silence of the darkness is broken in the
mountains by the low monotonous whistle of the pigmy-collared owlet.

The eye equally with the ear testifies to the traveller that when
he has reached an altitude of 5000 feet he has entered another avian
realm. The golden-backed woodpecker, the green bee-eater, the ``blue
jay'' or roller, the paddy bird, the Indian and the magpie-robin, most
familiar birds of the plains, are no longer seen. Their places are
taken by the blue-magpies, the beautiful verditer flycatcher, the
Himalayan and the black-headed jays, the black bulbul, and tits of
several species.

All the birds, it is true, are not new. Some of our familiar friends
of the plains are still with us. There are the kite, the scavenger
vulture, the common myna, and a number of others, but these are the
exceptions which prove the rule.

Scientific ornithologists recognise this great difference between
the two faunas, and include the Himalayas in the Palæarctic region,
while the plains form part of the Oriental region.

The chief things which affect the distribution of birds appear to
be food-supply and temperature. Hence it is evident that in the
Himalayas the avifauna along the snow-line differs greatly from that
of the low, warm valleys. The range of temperature in all parts of
the hills varies greatly with the season. At the ordinary hill
stations the minimum temperature in the summer is sometimes as high
as 70 degrees, while in the winter it may drop to 23 degrees F. Thus
in midwinter many of the birds which normally live near the snow-line
at 12,000 feet descend to 7000 or 6000 feet, and not a few hill birds
leave the Himalayas for a time and tarry in the plains until the
severity of the winter has passed away.
