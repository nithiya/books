\section*{THE CRATEROPODIDÆ OR BABBLER FAMILY}

The Crateropodidæ form a most heterogeneous collection of birds,
including, as they do, such divers fowls as babblers,
whistling-thrushes, bulbuls, and white-eyes. Whenever a systematist
comes across an Asiatic bird of which he can make nothing, he classes
it among the Crateropodidæ. This is convenient for the systematist,
but embarrassing for the naturalist.

The most characteristic members of the family are those ugly, untidy,
noisy earth-coloured birds which occur everywhere in the plains, and
always go about in little companies, whence their popular name ``seven
sisters.''

To men of science these birds are known as babblers. Babblers proper
are essentially birds of the plains. In the hills they are replaced
by their cousins, the laughing-thrushes. Laughing-thrushes are
merely glorified babblers. The Himalayan streaked laughing-thrush
(\textit{Trochalopterum lineatum}) is one of the commonest of the birds of
our hill stations. It is a reddish brown fowl, about eight inches
long. Each of its feathers has a black shaft; it is these dark shafts
that give the bird its streaked appearance. Its chin, throat, and
breast are chestnut-red, and on each cheek there is a patch of similar
hue. The general appearance of the streaked laughing-thrush is that
of one of the seven sisters who is wearing her best frock. Like their
sisters of the plains, Himalayan streaked laughing-thrushes go about
in small flocks and are exceedingly noisy. Sometimes a number of them
assemble, apparently for the sole purpose of holding a speaking
competition. They are never so happy as when thus engaged.

Streaked laughing-thrushes frequent gardens, and, as they are
inordinately fond of hearing their own voices, it is certainly not
their fault if they escape observation. By way of a nest they build
a rough-and-ready cup-shaped structure in a low bush or on the ground;
but, as Hume remarked, ``the bird, as a rule, conceals the nest so
well that, though a loose, and for the size of the architect, a large
structure, it is difficult to find, even when one closely examines
the bush in which it is.''

Three other species of laughing-thrush must be numbered among common
birds of the Himalayas, although they, like the heroine of \textit{A Bad
Girl's Diary}, are often heard and not seen. The white-throated
laughing-thrush (\textit{Garrulax albigularis}) is a handsome bird larger
than a myna. Its general colour is rich olive brown. It has a black
eyebrow and shows a fine expanse of white shirt front. It goes about
in large flocks and continually utters a cry, loud and plaintive and
not in the least like laughter.

The remaining laughing-thrushes are known as the rufous-chinned
(\textit{Ianthocincla rufigularis}) and the red-headed (\textit{Trochalopterum
erythrocephalum}). The former may be distinguished from the
white-throated species by the fact that the lower part only of its
throat is white, the chin being red. The red-headed laughing-thrush
has no white at all in the under parts. The next member of the family
of the Crateropodidæ that demands our attention is the rusty-cheeked
scimitar-babbler (\textit{Pomatorhinus erythrogenys}).

Scimitar-babblers are so called because of the long, slender,
compressed beak, which is curved downwards like that of a sunbird.

Several species of scimitar-babbler occur in the Himalayas. The above
mentioned is the most abundant in the Western Himalayas. This species
is known as the \textit{Banbakra} at Mussoorie. Its bill is 1½ inch long.
The upper plumage is olive brown. The forehead, cheeks, sides of the
neck, and thighs are chestnut-red, as is a patch under the tail. The
chin and throat and the median portion of the breast and abdomen are
white with faint grey stripes. Scimitar-babblers have habits similar
to those of laughing-thrushes. They go about in pairs, seeking for
insects among fallen leaves. The call is a loud whistle.

Very different in habits and appearance from any of the babblers
mentioned above is the famous Himalayan whistling-thrush
(\textit{Myiophoneous temmincki}). To see this bird it is necessary to repair
to some mountain stream. It is always in evidence in the neighbourhood
of the dhobi's ghat at Naini Tal, and is particularly abundant on
the banks of the Kosi river round about Khairna. At first sight the
Himalayan whistling-thrush looks very like a cock blackbird. His
yellow bill adds to the similitude. It is only when he is seen with
the sun shining upon him that the cobalt blue patches in his plumage
are noticed. His habit is to perch on the boulders which are washed
by the foaming waters of a mountain torrent. On these he finds plenty
of insects and snails, which constitute the chief items on his menu.
He pursues the elusive insect in much the same way as a wagtail does,
calling his wings to his assistance when chasing a particularly nimble
creature. He has the habit of frequently expanding his tail. This
species utters a loud and pleasant call, also a shrill cry like that
of the spotted forktail. All torrent-haunting birds are in the habit
of uttering such a note; indeed it is no easy task to distinguish
between the alarm notes of the various species that frequent mountain
streams.

Of very different habits is the black-headed sibia (\textit{Lioptila
capistrata}). This species is strictly arboreal. As mentioned
previously, it is often found in company with flocks of tits and other
gregarious birds. It feeds on insects, which it picks off the leaves
of trees. Its usual call is a harsh twitter. It is a reddish brown
bird, rather larger than a bulbul, with a black-crested head. There
is a white bar on the wing.

The Indian white-eye (\textit{Zosterops palbebrosa}) is not at all like any
of the babblers hitherto described. In size, appearance, and habits,
it approximates closely to the tits, with which it often consorts.
Indeed, Jerdon calls the bird the white-eyed tit. It occurs in all
well-wooded parts of the country, both in the plains and the hills.
No bird is easier to identify. The upper parts are greenish yellow,
and the lower bright yellow, while round the eye runs a broad
conspicuous ring of white feathers, whence the popular names of the
species, white-eye and spectacle-bird. Except at the breeding season,
it goes about in flocks of considerable size. Each individual utters
unceasingly a low, plaintive, sonorous, cheeping note. As was stated
above, all arboreal gregarious birds have this habit. It is by means
of this call note that they keep each other apprised of their
whereabouts. But for such a signal it would scarcely be possible for
the flock to hold together. At the breeding season the cock white-eye
acquires an unusually sweet song. The nest is an exquisite little
cup, which hangs, like a hammock, suspended from a slender forked
branch. Two pretty pale blue eggs are laid.

A very diminutive member of the babbler clan is the fire-cap
(\textit{Cephalopyrus flammiceps}). The upper parts of its plumage are olive
green; the lower portions are golden yellow. In the cock the chin
is suffused with red. The cock wears a further ornament in the shape
of a cap of flaming red, which renders his identification easy.

Until recently all ornithologists agreed that the curious
starling-like bird known as the spotted-wing (\textit{Psaroglossa
spiloptera}) was a kind of aberrant starling, but systematists have
lately relegated it to the Crateropodidæ. At Mussoorie the natives
call it the \textit{Puli}. Its upper parts are dark grey spotted with black.
The wings are glossy greenish black with white spots. The lower parts
are reddish. A flock of half-a-dozen or more birds having a
starling-like appearance, which twitter like stares and keep to the
topmost branches of trees, may be set down safely as spotted-wings.

We now come to the last of the Crateropodidæ--the bulbuls. These birds
are so different from most of their brethren that they are held to
constitute a sub-family. I presume that every reader is familiar with
the common bulbul of the plains. To every one who is not, my advice
is that he should go into the verandah in the spring and look among
the leaves of the croton plants. The chances are in favour of this
search leading to the discovery of a neat cup-shaped nest owned by
a pair of handsome crested birds, which wear a bright crimson patch
under the tail, and give forth at frequent intervals tinkling notes
that are blithe and gay.

Both the species of bulbul common in the plains ascend the lower ranges
of the Himalayas. These are the Bengal red-vented bulbul (\textit{Molpastes
bengalensis}) and the Bengal red-whiskered bulbul (\textit{Otocompsa
emeria}).

The addition of the adjective ``Bengal'' is important, for every
province of India has its own special species of bulbul.

The Molpastes bulbul is a bird about half as big again as the sparrow,
but with a longer tail. The black head is marked by a short crest.
The cheeks are brown. There is a conspicuous crimson patch under the
tail. The remainder of the plumage is brown, but each feather on the
body is margined with creamy white, so that the bird is marked by
a pattern that is, as ``Eha'' pointed out, not unlike the scales on
a fish. Both ends of the tail feathers are creamy white.

Otocompsa is a far more showy bird. The crest is long and pointed
and curves forward a little over the bill. There is the usual crimson
patch under the tail and another on each cheek. The rest of the cheek
is white, as is the lower plumage. A black necklace, interrupted in
front, marks the junction of the throat and the breast. Neither of
these bulbuls ascends the hills very high, but I have seen the former
at the Brewery below Naini Tal.

The common bulbul of the Himalayas is the white-cheeked species
(\textit{Molpastes leucogenys}). This bird, which is very common at Almora,
has the habits of its brethren in the plains. Its crest is pointed
and its cheeks are white like those of an Otocompsa bulbul. But it
has rather a weedy appearance and lacks the red feathers on the sides
of the head. The patch of feathers under the tail is bright
sulphur-yellow instead of crimson.

The only other species of bulbul commonly seen in the hills is a very
different bird. It is known as the black bulbul (\textit{Hypsipetes
psaroides}).

The bulbuls that we have been considering are inoffensive little birds
which lead quiet and respectable lives. Not so the black bulbuls.
These are aggressive, disreputable-looking creatures which go about
in disorderly, rowdy gangs.

The song of most bulbuls is a medley of pleasant tinkling notes; the
cries of the black bulbuls are harsh and unlovely.

Black bulbuls look black only when seen from a distance. When closely
inspected their plumage is seen to be dark grey. The bill and legs
are red. The crest, I regret to say, usually looks the worse for wear.
Black bulbuls seem never to descend to the ground. They keep almost
exclusively to tops of lofty trees. They are very partial to the nectar
enclosed within the calyces of rhododendron flowers. A party of half
a dozen untidy black birds, with moderately long tails, which keep
to the tops of trees and make much noise, may with certainty be set
down as black bulbuls.

These curious birds form the subject of a separate essay.
