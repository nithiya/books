\section*{THE CUCULIDÆ OR CUCKOO FAMILY}

It is not possible for anyone of sound hearing to be an hour in a
hill station in the early summer without being aware of the presence
of cuckoos. The Himalayas literally teem with them. From March to
June, or even July, the cheerful double note of the common cuckoo
(\textit{Cuculus canorus}) emanates from every second tree. This species,
as all the world knows, looks like a hawk and flies like a hawk.

According to some naturalists, the cuckoo profits by its similarity
to a bird of prey. The little birds which it imposes upon are supposed
to fly away in terror when they see it, thus allowing it to work
unmolested its wicked will in their nests. My experience is that
little birds have a habit of attacking birds of prey that venture
near their nest. The presence of eggs or young ones makes the most
timid creatures as bold as the proverbial lion. I therefore do not
believe that these cuckoos which resemble birds of prey derive any
benefit therefrom.

The hen European cuckoo differs very slightly from the cock. In some
species, as, for example, the famous ``brain-fever bird''
(\textit{Hierococcyx varius}), there is no external difference between the
sexes, while in others, such as the Indian koel (\textit{Eudynamis honorata}),
and the violet cuckoo (\textit{Chrysococcyx xanthorhynchus}), the sexes are
very dissimilar. I commend these facts to the notice of those who
profess to explain sexual dimorphism (the different appearance of
the sexes) by means of natural or sexual selection. The comfortable
theory that the hens are less showily coloured than the cocks, because
they stand in greater need of protective colouring while sitting on
the nest, cannot be applied to the parasitic cuckoos, for these build
no nests, neither do they incubate their eggs.

In the Himalayas the common cuckoo victimises chiefly pipits, larks,
and chats, but its eggs have been found in the nests of many other
birds, including the magpie-robin, white-cheeked bulbul, spotted
forktail, rufous-backed shrike, and the jungle babbler.

The eggs of \textit{Cuculus canorus} display considerable variation in
colour. Those who are interested in the subject are referred to Mr.
Stuart Baker's papers on the Oology of the Indian Cuckoos in Volume
XVII of the \textit{Journal of the Bombay Natural History Society}.

It often happens that the eggs laid by the cuckoo are not unlike those
of the birds in the nests of which they are deposited. Hence, some
naturalists assert that the cuckoo, having laid an egg, flies about
with it in her bill until she comes upon a clutch which matches her
egg. Perhaps the best reply to this theory is that such refinement
on the part of the cuckoo is wholly unnecessary. Most birds, when
seized by the mania of incubation, will sit upon anything which even
remotely resembles an egg.

Mr. Stuart Baker writes that he has not found that there is any proof
of the cuckoo trying to match its eggs with those of the intended
foster-mother, or that it selects a foster-mother whose eggs shall
match its own. He adds that not one of his correspondents has advanced
this suggestion, and states that he has little doubt that convenience
of site and propinquity to the cuckoo about to lay its eggs are the
main requisitions.

Almost indistinguishable from the common cuckoo in appearance is the
Himalayan cuckoo (\textit{Cuculus saturatus}). The call of this bird, which
continues later in the year than that of the common cuckoo, is not
unlike the \textit{whoot-whoot-whoot} of the crow-pheasant or coucal.
Perhaps it is even more like the \textit{uk-uk-uk} of the hoopoe repeated
very loudly. It may be syllabised as \textit{cuck-hoo-hoo-hoo-hoo}. Not very
much is known about the habits of this species. It is believed to
victimise chiefly willow-warblers.

The Indian cuckoo (\textit{Cuculus micropterus}) resembles in appearance
the two species already described. Blanford speaks of its call as
a fine melodious whistle. I would not describe the note as a whistle.
To me it sounds like \textit{wherefore}, \textit{wherefore}, impressively and
sonorously intoned. The vernacular names \textit{Boukotako} and
\textit{Kyphulpakka} are onomatopoetic, as is Broken Pekoe Bird, by which
name the species is known to many Europeans.

Last, but not least of the common Himalayan cuckoos, are the famous
brain-fever birds, whose crescendo \textit{brain-fever}, \textit{BRAIN-FEVER},
\textit{BRAIN-FEVER}, which is shrieked at all hours of the day and the night,
has called forth untold volumes of awful profanity from jaded
Europeans living in the plains, and has earned the highest encomiums
of Indians.

There are two species of brain-fever bird that disport themselves
in the Himalayas. These are known respectively as the large and the
common hawk-cuckoo (\textit{Hierococcyx sparverioides} and \textit{H. varius}).
I do not profess to distinguish with certainty between the notes of
these two birds, but am under the impression that the larger form
is the one that makes itself heard at Naini Tal and Mussoorie.

The Indian koel (\textit{Eudynamis honorata}) is not to be numbered among
the common birds of the Himalayas. Its noisy call \textit{kuil}, \textit{kuil},
\textit{kuil}, which may be expressed by the words \textit{you're-ill},
\textit{you're-ill}, \textit{who-are-you?} \textit{who-are-you?} is heard throughout the
sub-Himalayan regions in the early summer, and I have heard it as
high up as Rajpur below Mussoorie, but have not noticed the bird at
any of the hill stations except Almora. As has already been stated,
the avifauna of Almora, a little station in the inner hills nearly
forty miles from the plains, is a very curious one. I have not only
heard the koel calling there, but have seen a young koel being fed
by crows. Now, at Almora alone of the hill stations does \textit{Corvus
splendens}, the Indian house-crow, occur, and this is the usual victim
of the koel. I would therefore attribute the presence of the koel
at Almora and its absence from other hill stations to the fact that
at Almora alone the koel's dupe occurs.
