# books <!-- omit in toc -->

A compilation of interesting public domain books from [Project Gutenberg](https://www.gutenberg.org/). The following PDF books have been built using LaTeX so far:

- [Draft] Birds of the Indian Hills [[download](https://gitlab.com/nithiya/books/-/jobs/artifacts/master/raw/books/birds-of-the-indian-hills/birds-of-the-indian-hills.pdf?job=pdf)]
- Ivanhoe [[download](https://gitlab.com/nithiya/books/-/jobs/artifacts/master/raw/books/ivanhoe/ivanhoe.pdf?job=pdf)]
- Robin Hood [[download](https://gitlab.com/nithiya/books/-/jobs/artifacts/master/raw/books/robin-hood/robin-hood.pdf?job=pdf)]
- Viking Tales [[download](https://gitlab.com/nithiya/books/-/jobs/artifacts/master/raw/books/viking-tales/viking-tales.pdf?job=pdf)]

## Table of contents <!-- omit in toc -->

- [Getting eBooks](#getting-ebooks)
- [Converting eBooks to LaTeX](#converting-ebooks-to-latex)
- [LaTeX requirements](#latex-requirements)
- [Compilation](#compilation)
- [LaTeX files](#latex-files)
- [To-do](#to-do)
- [License](#license)

## Getting eBooks

The main Project Gutenberg website is ["intended for human users only"](https://www.gutenberg.org/policy/terms_of_use.html). However, there are exceptions, as outlined [here](https://www.gutenberg.org/policy/robot_access.html), through the use of [`wget`](https://www.gnu.org/software/wget/). One can alternatively use a Project Gutenberg mirror site.

Humans can access the text file for an eBook using the URL: `${domain}/files/${ebooknumber}/${ebooknumber}${encoding}.txt`, where:

- `${domain}` is `https://www.gutenberg.org`
- `${ebooknumber}` is the eBook number
- `${encoding}` is the text file's character set encoding; e.g. `-0` for UTF-8, `-8` for ISO-8859-1; not applicable to ASCII files

If using a mirror site, eBooks can be obtained through the following URL:

- for a 5-digit eBook number; e.g. `12345`: `${domain}/1/2/3/4/12345${encoding}.txt`
- for a 1-digit eBook number; e.g. `9`: `${domain}/0/9${encoding}.txt`

`${domain}` here refers to the mirror's domain. See the list of mirror sites [here](https://www.gutenberg.org/dirs/MIRRORS.ALL).

## Converting eBooks to LaTeX

For eBook text files with a character set encoding other than UTF-8, I first converted them to UTF-8 using [`iconv`](https://en.wikipedia.org/wiki/Iconv). To convert ISO-8859-1 to UTF-8:

```sh
iconv -f ISO-8859-1 -t UTF-8 ${ebooknumber}-8.txt -o ${ebooknumber}-0.txt
```

The text files are then parsed into `.tex` using mostly find and replace; e.g. replace each odd and even occurrence of `_`, which is used to format italics, with `\textit{` and `}`, respectively. This can be achieved using [`sed`](https://www.gnu.org/software/sed/). This is the current `sed` code, which could be improved in the future:

```sh
sed ':a;N;$!ba;s|_\([^_]*\)_|\\textit{\1}|g' ${ebooknumber}-0.txt > ${ebooknumber}.tex

sed -i -e ':a;N;$!ba;s|"\([^"]*\)"|``\1'"''"'|g' -e 's|#|\\#|g' -e 's|&|\\&|g' ${ebooknumber}.tex
```

Other forms of modification are done manually.

## LaTeX requirements

This section lists the LaTeX packages used to build these PDF files. These packages are available on [CTAN](https://www.ctan.org/).

Some of these packages have additional dependencies not listed here. Therefore, it is recommended to use a TeX distribution, such as [TeX Live](https://tug.org/texlive/), to ensure all requirements are satisfied.

Document class: [book](https://www.ctan.org/pkg/book)

Packages:

- [background](https://www.ctan.org/pkg/background) (for drafts)
- [caption](https://www.ctan.org/pkg/caption)
- [chngcntr](https://www.ctan.org/pkg/chngcntr)
- [datetime2](https://www.ctan.org/pkg/datetime2)
- [enumitem](https://www.ctan.org/pkg/enumitem)
- [etoolbox](https://www.ctan.org/pkg/etoolbox)
- [fancyhdr](https://www.ctan.org/pkg/fancyhdr)
- [fix-cm](https://www.ctan.org/pkg/fix-cm)
- [geometry](https://www.ctan.org/pkg/geometry)
- [graphicx](https://www.ctan.org/pkg/graphicx)
- [hyperref](https://www.ctan.org/pkg/hyperref)
- [lettrine](https://www.ctan.org/pkg/lettrine)
- [multicol](https://www.ctan.org/pkg/multicol)
- [verse](https://www.ctan.org/pkg/verse)
- [titlesec](https://www.ctan.org/pkg/titlesec)
- [tocloft](https://www.ctan.org/pkg/tocloft)
- [xcolor](https://www.ctan.org/pkg/xcolor)

Fonts:

- [Carrick Caps](https://www.ctan.org/pkg/initials)
- [cinzel](https://www.ctan.org/pkg/cinzel)
- [ebgaramond](https://www.ctan.org/pkg/ebgaramond)
- [Goudy Initialen](https://www.ctan.org/pkg/initials)
- [Zallman Caps](https://www.ctan.org/pkg/initials)

## Compilation

The PDF files are built using either [XeLaTeX](http://xetex.sourceforge.net/) or [LuaLaTeX](http://luatex.org/) via [Arara](https://gitlab.com/islandoftex/arara):

```sh
cd books
for dir in */;
do cd ${dir%%/} &&
arara ${dir%%/}.tex &&
cd ..;
done
```

Note that Arara requires a [Java](https://jdk.java.net/) installation. Running either XeLaTeX or LuaLaTeX twice is a non-Java alternative.

Another option is to use the latest TeX Live Docker image by [Island of TeX](https://gitlab.com/islandoftex/images/texlive), which can also be used with GitLab CI. The following is a minimal example of a valid `.gitlab-ci.yml` configuration:

```yml
image: registry.gitlab.com/islandoftex/images/texlive:latest

build:
  before_script:
    - cd books
  script:
    - for dir in */;
      do cd ${dir%%/} &&
      arara ${dir%%/}.tex &&
      cd ..;
      done
  artifacts:
    paths:
      - "**/*.pdf"
```

The resulting PDF files will be available as artifacts once the build is complete. See this [TUGboat article](https://tug.org/TUGboat/tb40-3/tb126island-docker.pdf) for more information.

## LaTeX files

- style file: [template.sty](template.sty)
- `.tex` files of books:
  - [ivanhoe.tex](books/ivanhoe/ivanhoe.tex)
  - [robin-hood.tex](books/robin-hood/robin-hood.tex)
  - [viking-tales.tex](books/viking-tales/viking-tales.tex)

## To-do

- fix underfull / overfull boxes

## License

- Project Gutenberg eBooks are licensed under the Project Gutenberg License. See the [Project Gutenberg website](https://www.gutenberg.org/policy/permission.html) for more information.
- Code is licensed under the [MIT License](https://opensource.org/licenses/MIT).
